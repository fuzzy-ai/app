Hacking fuzzy.ai
================

If you're working on the fuzzy.ai Web front-end, you can set up a dev
environment by following these steps.

### System Setup

These are the steps you should only have to do once on your machine to get all of the tools in place.

1. Clone the fuzzyio/home project from Github. The Git URL is `git@github.com:fuzzy-ai/home.git` . It is a private repository so you need to have an SSH key set up with Github.

1. Install Node.js. Follow the instructions at http://nodejs.org/

   You should be able to run `node --version` and `npm --version` from the command line.

1. Install Coffeescript. From the command line, run:

        npm install -g coffee-script

   You should be able to run `coffee --version` and `cake` from the
   command line.

1. Install Docker. See https://docs.docker.com/engine/installation/
   for details.

   You should be able to run `docker -v` from the command line.

1. Install Docker Compose. See https://docs.docker.com/compose/install/ for details.

   You should be able to run `docker-compose -v` from the command line.

1. Install and authenticate the Google Cloud SDK. See: https://cloud.google.com/sdk/

   Once the SDK is installed, you should be able to run the following to authenticate (using your fuzzy account):

        gcloud auth login

1. Authenticate docker to use the gcr.io repository:

        docker login -u oauth2accesstoken -p "$(gcloud auth print-access-token)" https://gcr.io

1. *OSX Only* Create / run a docker machine:

        docker-machine create --driver=virtualbox fuzzy

### Working Steps

The following steps you'll need whenever you're working on the code. Remember to `git pull` so that your code is up to date!

1. *OSX Only* Ensure your docker machine is running:

        docker-machine start fuzzy

1. *OSX Only* Set the docker machine environment variables.

        eval $(docker-machine env fuzzy)

1. Make sure you have the latest images. From the command line, run:

        docker-compose pull

1. Start the microservices. From the command line, run:

        docker-compose up -d

1. Install all the dependencies. From the command line, run:

        npm install

1. Launch the development server. In the main directory of the home project, run:

        cake dev

1. You can now view the site at [http://localhost:8000](http://localhost:8000)

1. Edit what you want. Any scss or cjsx file changes should live reload. :rocket:

### Running with Docker

If you want to run locally with the Docker container, use this command:

  docker-compose -f docker-compose.yml -f home.yml up -d

That will run the microservices, build the home image, and run the home image.

### Developer tools

There are a few browser extensions that make working with react/redux a bit easier:

* https://github.com/facebook/react-devtools
* https://github.com/gaearon/redux-devtools

### Coding Standards

Install and run coffeelint

Install via `npm install -g coffeelint`

Run via `coffeelint --ext cjsx,coffee src`

### Configuration variables

This code accepts the following environment variables for configuration.

* WEB_PORT: Port to listen on, default = 80
* WEB_HOSTNAME: Hostname to use for URLs; default is 'localhost'
* WEB_ADDRESS: Address to listen on; default is '0.0.0.0'
* WEB_KEY: SSL key (not the name of the file; the full key!). Default is null.
* WEB_CERT: SSL cert; default = null
* WEB_AUTH_SERVER: root URL of auth microservice
* WEB_AUTH_KEY: OAuth token to use for auth microservice
* WEB_SESSION_SECRET: secret to use for storing web sessions. Default is
  garbage.
* WEB_API_SERVER: root URL of API server, preferably on the internal interface
* WEB_API_KEY: OAuth token to use for API server
* WEB_LOG_LEVEL: Log level to use. Default is "info".
* WEB_STATS_SERVER: root URL of the stats microservice
* WEB_STATS_KEY: OAuth token for the stats microservice
* WEB_PLAN_SERVER: root URL of the plan microservice
* WEB_PLAN_KEY: OAuth token for the plan microservice
* WEB_PAYMENTS_SERVER: root URL of the payments microservice
* WEB_PAYMENTS_KEY: OAuth token for the payments microservice
* WEB_TEMPLATES_SERVER: root URL of the templates microservice
* WEB_TEMPLATES_KEY: OAuth token for the templates microservice
* WEB_CA_CERT: A full cert for a private CA. We don't use this anymore.
* DRIVER: Databank driver to use for session storage.
* PARAMS: Databank driver params for session storage.
* CLEANUP: Session cleanup frequency in microseconds. Default is 1 hour.
* SLACK_HOOK: Hook to post error info to.
* API_CLIENT_CACHE_SIZE: How big the client cache should be. Default 50.
* WEB_CLIENT_TIMEOUT: How long to wait when making WebClient requests.
* REDIRECT_TO_HTTPS: Whether to redirect from http: to https:. The Kubernetes
  load balancer can't handle this so we have to do it ourselves. Default false.
* WEB_SESSION_MAX_AGE: Max age for the session. Default is 30 days.
* WEB_SESSION_SECURE: Whether the web session cookie should be secure (only
  https). Default is false.
