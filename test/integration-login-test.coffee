# integration-login-test.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

vows = require 'perjury'
assert = vows.assert

{ browser, apiBatch } = require './apibatch'

vows
  .describe("Successful /login")
  .addBatch apiBatch
    'and we visit /login':
      topic: ->
        browser.visit '/login', @callback
        undefined
      'it works': ->
        browser.assert.success()
      'it has an email field': ->
        browser.assert.input '#email', ''
      'it has a password field': ->
        browser.assert.input '#password', ''
      'it has a login button': ->
        browser.assert.text 'button.btn', 'Login'
      'it has a forgot password link': ->
        browser.assert.link '.form_user_request', 'Forgot password?', 'reset'
      'and we login':
        topic: ->
          browser.fill 'email', 'test@example.com'
            .fill 'password', 'testing123'
            .pressButton 'Login', @callback
          undefined
        'it works': ->
          browser.assert.success()
        'it has a dashboard element': ->
          browser.assert.element '.dashboard'
  .export(module)
