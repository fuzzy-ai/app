# integration-tos-test.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

vows = require 'perjury'
assert = vows.assert

{ browser, apiBatch } = require './apibatch'

vows
  .describe("GET /tos")
  .addBatch apiBatch
    'and we visit /tos':
      topic: ->
        browser.visit '/tos', @callback
        undefined
      'it works': ->
        browser.assert.success()
      'it has a TOS header': ->
        browser.assert.text('h2', 'Terms of Service')
  .export(module)
