# integration-agents-new-test.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

vows = require 'perjury'
assert = vows.assert

{ browser, loginBatch } = require './apibatch'

vows
  .describe("Test /agents/new/empty")
  .addBatch loginBatch
    'and we visit /agents/new/empty':
      topic: ->
        browser.visit '/agents/new/empty', @callback
        undefined
      'it works': ->
        browser.assert.success()
      'it has an #output field': ->
        browser.assert.input('#output', '')
      'it has a range #from field': ->
        browser.assert.input('#from', 0)
      'it has a range #to field': ->
        browser.assert.input('#to', 100)
      'it has a submit button': ->
        browser.assert.text('button.btn', 'Save')
      'and we submit a filled form':
        topic: ->
          browser.fill '#output', 'Test Agent'
            .fill '#from', 0
            .fill '#to', 100
            .pressButton 'Save', @callback
          undefined
        'we are redirected to agent page': ->
          browser.assert.element('.agent-edit')

  .export(module)
