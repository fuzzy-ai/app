# integration-login-fail-test.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

vows = require 'perjury'
assert = vows.assert

{ browser, apiBatch } = require './apibatch'

vows
  .describe("Unsuccessful /login")
  .addBatch apiBatch
    'and we visit /login':
      topic: ->
        browser.visit '/login', @callback
        undefined
      'it works': ->
        browser.assert.success()
      'and we submit with an invalid email':
        topic: ->
          browser.fill 'email', 'invalid@example.com'
            .fill 'password', 'testing123'
            .pressButton 'Login', @callback
          undefined
        'it stays on the login page': ->
          browser.assert.url(pathname: '/login')
        'an error message is shown': ->
          browser.assert.element('div.error')
        'and we submit with an invalid password':
          topic: ->
            browser.fill 'email', 'test@example.com'
              .fill 'password', 'invalid-password'
              .pressButton 'Login', @callback
            undefined
          'it stays on the login page': ->
            browser.assert.url pathname: '/login'
          'an error message is shown': ->
            browser.assert.element('div.error')
  .export(module)
