# statsclient.coffee
# Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

qs = require 'querystring'

_ = require 'lodash'
MicroserviceClient = require '@fuzzy-ai/microservice-client'

lpad = (value, padding) ->
  zeroes = "0"
  zeroes += "0" for i in [1..padding]

  (zeroes + value).slice(padding * -1)

class StatsClient extends MicroserviceClient

  getAgentStats: (agentID, year, month, callback) ->
    @get "/agent/#{agentID}/#{lpad(year, 4)}/#{lpad(month, 2)}", callback

  getUserStats: (userID, year, month, callback) ->
    @get "/user/#{userID}/#{lpad(year, 4)}/#{lpad(month, 2)}", callback

  getUserLogs: (userID, offset, limit, callback) ->

    if !callback?
      callback = limit
      limit = 20

    if !callback?
      callback = offset
      offset = 0

    params = qs.stringify {offset: offset, limit: limit}

    @get "/user/#{userID}/stream?#{params}", callback

  getAgentLogs: (agentID, offset, limit, callback) ->

    if !callback?
      callback = limit
      limit = 20

    if !callback?
      callback = offset
      offset = 0

    params = qs.stringify {offset: offset, limit: limit}

    @get "/agent/#{agentID}/stream?#{params}", callback

  getLog: (reqID, callback) ->

    @get "/evaluation/#{reqID}", callback
    
module.exports = StatsClient
