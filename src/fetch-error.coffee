# fetch-error.coffee
# Error class for when a fetch() call goes wrong
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved.

class FetchError extends Error
  constructor: (response, json) ->
    @status = response.status
    @message = json?.message

module.exports = FetchError
