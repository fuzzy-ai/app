{ createStore, compose, applyMiddleware } = require 'redux'
{ routerMiddleware } = require 'react-router-redux'
{ createRequestMiddleware } = require 'redux-requests'
thunkMiddleware = require('redux-thunk').default

reducers = require './reducers'

module.exports = (history, initialState) ->
  return createStore(
    reducers,
    initialState,
    compose(
      applyMiddleware(
        thunkMiddleware,
        createRequestMiddleware(),
        routerMiddleware(history)
      ),
      if window?.devToolsExtension?
        window.devToolsExtension()
      else
        (f) -> f
    )
  )
