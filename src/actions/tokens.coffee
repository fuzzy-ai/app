# src/actions/tokens.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

require 'es6-promise'
fetch = require 'isomorphic-fetch'

{ checkResponse } = require '.'

requestTokens = ->
  type: 'REQUEST_TOKENS'

receiveTokens = (tokens) ->
  type: 'RECEIVE_TOKENS'
  tokens: tokens

receiveTokensError = (err) ->
  type: 'RECEIVE_TOKENS_ERROR'
  error: err

exports.fetchTokens = ->
  (dispatch) ->
    dispatch(requestTokens())
    fetch('/data/tokens',
      credentials: 'same-origin',
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json')
    .then (response) -> checkResponse(response, dispatch)
    .then (json) ->
      dispatch receiveTokens(json.tokens)
    .catch (err) ->
      dispatch receiveTokensError(err)

addToken = ->
  type: 'ADD_TOKEN'

addTokenSuccess = (token) ->
  type: 'ADD_TOKEN_SUCCESS'
  token: token

addTokenError = (err) ->
  type: 'ADD_TOKEN_ERROR'
  error: err

exports.newToken = (data) ->
  (dispatch) ->
    dispatch(addToken())
    fetch('/data/tokens',
      method: 'post'
      credentials: 'same-origin',
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json'
      body: JSON.stringify data)
    .then (response) -> checkResponse(response, dispatch)
    .then (json) ->
      dispatch addTokenSuccess(json.token)
    .catch (err) ->
      dispatch addTokenError(err)
