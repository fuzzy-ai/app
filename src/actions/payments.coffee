require 'es6-promise'
fetch = require 'isomorphic-fetch'
{ push } = require 'react-router-redux'

{ setUser } = require './user'
{ checkResponse } = require '.'


beginCustomer = ->
  return { type: 'CREATE_CUSTOMER' }

exports.customerError = customerError = (message) ->
  return { type: 'CREATE_CUSTOMER_ERROR', message: message }

exports.createCustomer = (data) ->
  (dispatch) ->
    dispatch(beginCustomer())

    fetch('/upgrade',
      method: 'post'
      credentials: 'same-origin',
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json'
      body: JSON.stringify data)
    .then (response) -> checkResponse(response, dispatch)
    .then (json) ->
      dispatch(setUser(json.user))
      dispatch(push('/upgraded'))
    .catch (err) ->
      dispatch(customerError(err))

beginGetCustomer = ->
  type: 'GET_CUSTOMER'

getCustomerSuccess = (data) ->
  type: 'GET_CUSTOMER_SUCCESS'
  customer: data.customer

getCustomerError = (err) ->
  type: 'GET_CUSTOMER_ERROR'
  error: err

exports.fetchCustomer = ->
  (dispatch) ->
    dispatch(beginGetCustomer())

    fetch("/data/customer",
      credentials: 'same-origin'
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json')
    .then (response) -> checkResponse(response, dispatch)
    .then (json) ->
      dispatch getCustomerSuccess json
    .catch (err) ->
      dispatch getCustomerError err

beginUpdateCustomer = ->
  type: 'UPDATE_CUSTOMER'

updateCustomerSuccess = (data) ->
  type: 'UPDATE_CUSTOMER_SUCCESS'
  customer: data.customer

updateCustomerError = (err, data) ->
  type: 'UPDATE_CUSTOMER_ERROR'
  error: err
  data: data

exports.updateCustomer = (data) ->
  (dispatch) ->
    dispatch(beginUpdateCustomer())

    fetch("/data/customer",
      method: 'put'
      credentials: 'same-origin'
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json'
      body: JSON.stringify data)
    .then (response) -> checkResponse(response, dispatch)
    .then (json) ->
      if json.user
        dispatch setUser(json.user)
      dispatch updateCustomerSuccess json
    .catch (err) ->
      dispatch updateCustomerError err, data

beginGetInvoices = ->
  type: "GET_INVOICES"

getInvoicesSuccess = (data) ->
  type: "GET_INVOICES_SUCCESS"
  invoices: data.invoices

getInvoicesError = (err) ->
  type: "GET_INVOICES_ERROR"
  error: err

exports.fetchInvoices = ->
  (dispatch) ->
    dispatch beginGetInvoices()

    fetch("/data/invoices",
      credentials: 'same-origin'
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json')
    .then (response) -> checkResponse(response, dispatch)
    .then (json) ->
      dispatch getInvoicesSuccess json
    .catch (err) ->
      dispatch getInvoicesError err

beginGetInvoice = ->
  type: "GET_INVOICE"

getInvoiceSuccess = (data) ->
  type: "GET_INVOICE_SUCCESS"
  currentInvoice: data.invoice

getInvoiceError = (err) ->
  type: "GET_INVOICE_ERROR"
  error: err

exports.fetchInvoice = (invoiceId) ->
  (dispatch) ->
    dispatch beginGetInvoice()

    fetch("/data/invoices/#{invoiceId}"
      credentials: 'same-origin'
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json')
    .then (response) -> checkResponse(response, dispatch)
    .then (json) ->
      dispatch getInvoiceSuccess json
    .catch (err) ->
      dispatch getInvoiceError err
