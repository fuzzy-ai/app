# src/actions/agent.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

require 'es6-promise'
_ = require 'lodash'
fetch = require 'isomorphic-fetch'
{ push } = require 'react-router-redux'
{ attemptRequest } = require 'redux-requests'
{ checkResponse } = require '.'

exports.newAgent = (agent, redirect = true) ->
  (dispatch, getState) ->
    url = '/data/agents'

    attemptRequest(url, {
      begin: ->
        type: 'AGENT_CREATE_BEGIN'
      success: (response) ->
        window?.Intercom? 'trackEvent', 'agent-new',
          agentID: response.agent.id
          agentName: response.agent.name
        window?.ga? 'send',
          hitType: 'event'
          eventCategory: 'Agent'
          eventAction: 'new'
        if redirect
          dispatch(push(response.url))
        {
          type: 'AGENT_CREATE_SUCCESS'
          agent: response.agent
        }
      failure: (error) ->
        type: 'AGENT_CREATE_ERROR'
        agent: agent
        error: err
    }, ->
      fetch(url,
        method: 'post'
        credentials: 'same-origin'
        headers:
          'Accept': 'application/json'
          'Content-Type': 'application/json'
        body: JSON.stringify agent)
      .then (response) -> checkResponse(response, dispatch)
    , dispatch)

requestAgents = ->
  return { type: 'REQUEST_AGENTS' }

receiveAgents = (agents) ->
  type: 'RECEIVE_AGENTS'
  agents: agents

requestAgentsError = (err) ->
  type: 'REQUEST_AGENTS_ERROR'
  error: err

exports.fetchAgents = ->
  (dispatch) ->
    dispatch(requestAgents())
    fetch("/data/agents",
      credentials: 'same-origin',
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json')
    .then (response) -> checkResponse(response, dispatch)
    .then (json) ->
      dispatch receiveAgents(json)
    .catch (err) ->
      dispatch requestAgentsError(err)

exports.fetchAgent = (agentID) ->
  (dispatch) ->
    url = "/data/agent/#{agentID}"

    attemptRequest(url, {
      begin: ->
        type: 'REQUEST_AGENT_BEGIN'
      success: (response) ->
        type: 'REQUEST_AGENT_SUCCESS'
        agent: response
      failure: (error) ->
        type: 'REQUEST_AGENT_ERROR'
        error: error
        agentID: agentID
    }, ->
      fetch(url,
        credentials: 'same-origin',
        headers:
          'Accept': 'application/json'
          'Content-Type': 'application/json')
      .then (response) -> checkResponse(response, dispatch)
    , dispatch)

exports.storeAgent = storeAgent = (agent, originalAgent) ->
  (dispatch, getState) ->
    url = "/data/agent/#{agent.id}"

    attemptRequest(url, {
      begin: ->
        type: 'STORE_AGENT_BEGIN'
      success: (response) ->
        type: 'STORE_AGENT_SUCCESS'
        agent: response.agent
      failure: (error) ->
        type: 'STORE_AGENT_ERROR'
        error: error
        agent: agent
    }, ->
      fetch(url,
        method: 'put',
        credentials: 'same-origin',
        headers:
          'Accept': 'application/json'
          'Content-Type': 'application/json'
        body: JSON.stringify agent)
      .then (response) -> checkResponse(response, dispatch)
    , dispatch)

exports.addRule = (agent, newRule) ->
  (dispatch) ->
    newAgent = _.cloneDeep(agent)
    newAgent.parsed_rules.push newRule
    dispatch(storeAgent(newAgent, agent))

exports.editRule = (agent, rule, key) ->
  (dispatch) ->
    newAgent = _.cloneDeep(agent)
    newAgent.parsed_rules[key] = rule
    dispatch(storeAgent(newAgent, agent))

exports.deleteRule = (agent, key) ->
  (dispatch) ->
    newAgent = _.cloneDeep(agent)
    newAgent.parsed_rules.splice(key, 1)
    dispatch(storeAgent(newAgent, agent))

exports.deleteAgent = (agent) ->
  (dispatch, getState) ->
    url = "/data/agents/#{agent.id}"

    attemptRequest(url, {
      begin: ->
        type: 'DELETE_AGENT_BEGIN'
      success: (response) ->
        push('/')
      failure: (error) ->
        type: 'DELETE_AGENT_ERROR'
        error: error
        agent: agent
    }, ->
      fetch(url,
        method: 'delete'
        credentials: 'same-origin'
        headers:
          'Accept': 'application/json'
          'Content-Type': 'application/json')
      .then (response) -> checkResponse(response, dispatch)
    , dispatch)

exports.testAgentReset = (agent) ->
  type: 'TEST_AGENT_RESET'
  agent: agent

exports.testAgent = (agent, inputs) ->
  (dispatch, getState) ->
    url = "/data/agent/#{agent.id}"
    start = performance.now()

    attemptRequest(url, {
      begin: ->
        type: 'TEST_AGENT_BEGIN'
      success: (response) ->
        console.log response
        window?.Intercom? 'trackEvent', 'agent-test',
          agentID: agent.id
        window?.ga? 'send',
          hitType: 'event'
          eventCategory: 'Agent'
          eventAction: 'test'
        {
          type: 'TEST_AGENT_SUCCESS'
          agent: agent
          inputs: inputs
          outputs: _.omit(response, 'meta')
          meta: response.meta
          timing: performance.now() - start
        }
      failure: (error) ->
        type: 'TEST_AGENT_ERROR'
        agent: agent
        inputs: inputs
        error: error
    }, ->
      fetch(url, {
        method: 'post',
        credentials: 'same-origin',
        headers:
          'Accept': 'application/json'
          'Content-Type': 'application/json'
        body: JSON.stringify inputs})
      .then (response) -> checkResponse(response, dispatch)
    , dispatch)
