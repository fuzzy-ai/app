# src/actions/logs.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

require 'es6-promise'
_ = require 'lodash'
fetch = require 'isomorphic-fetch'
{ push } = require 'react-router-redux'

{ checkResponse } = require '.'

requestLogs = (offset, limit) ->
  type: 'REQUEST_LOGS'
  offset: offset
  limit: limit

receiveLogs = (offset, limit, logs) ->
  type: 'RECEIVE_LOGS'
  offset: offset
  limit: limit
  logs: logs

logsError = (offset, limit, err) ->
  type: 'LOGS_ERROR'
  offset: offset
  limit: limit
  error: err

requestAgentLogs = (offset, limit, agentID) ->
  type: 'REQUEST_AGENT_LOGS'
  offset: offset
  limit: limit
  agentID: agentID

exports.fetchLogs = (offset, limit) ->
  (dispatch) ->
    dispatch(requestLogs(offset, limit))
    fetch("/data/logs?offset=#{offset}&limit=#{limit}",
      credentials: 'same-origin',
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json')
    .then (response) -> checkResponse(response, dispatch)
    .then (json) -> dispatch(receiveLogs(offset, limit, json))
    .catch (err) -> dispatch(logsError(offset, limit, err))

exports.fetchAgentLogs = (offset, limit, agentID) ->
  (dispatch) ->
    dispatch(requestAgentLogs(offset, limit, agentID))
    fetch("/data/agent/#{agentID}/logs?offset=#{offset}&limit=#{limit}",
      credentials: 'same-origin',
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json')
    .then (response) -> checkResponse(response, dispatch)
    .then (json) -> dispatch(receiveLogs(offset, limit, json))
    .catch (err) -> dispatch(logsError(offset, limit, err))

requestLog = (reqID) ->
  type: 'REQUEST_LOG'
  reqID: reqID

receiveLog = (reqID, log) ->
  type: 'RECEIVE_LOG'
  reqID: reqID
  current: log

logError = (reqID, err) ->
  type: 'LOG_ERROR'
  reqID: reqID
  error: err

exports.fetchLog = (reqID) ->
  (dispatch) ->
    dispatch(requestLog(reqID))
    fetch("/data/log/#{reqID}",
      credentials: 'same-origin',
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json')
    .then (response) -> checkResponse(response, dispatch)
    .then (json) -> dispatch(receiveLog(reqID, json))
    .catch (err) -> dispatch(logError(reqID, err))
