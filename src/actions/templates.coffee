# src/actions/templates.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

require 'es6-promise'
_ = require 'lodash'
fetch = require 'isomorphic-fetch'

FetchError = require '../fetch-error'

requestTemplates = ->
  return { type: 'REQUEST_TEMPLATES' }

receiveTemplates = (templates) ->
  type: 'RECEIVE_TEMPLATES'
  templates: templates

templatesError = (err) ->
  type: 'TEMPLATES_ERROR'
  error: err

exports.fetchTemplates = ->
  (dispatch) ->
    dispatch(requestTemplates())
    fetch("/data/templates",
      credentials: 'same-origin',
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json'
    )
    .then (response) ->
      response.json().then (json) ->
        if response.ok
          dispatch receiveTemplates json
        else
          throw new FetchError response, json
    .catch (err) ->
      dispatch templatesError err
