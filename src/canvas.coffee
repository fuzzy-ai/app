# canvas.coffee
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved.

_ = require 'lodash'
pcentroid = require './centroid'

class Grid
  constructor: (@width, @height, @padding, @sets, @onChange) ->
    @colours = [
      [105, 78, 124]
      [83, 180, 196]
      [234, 70, 85]
      [69, 56, 75]
      [249, 86, 79]
      [105, 78, 124]
      [83, 180, 196]
      [225, 47, 62]
      [255, 250, 227]
      [231, 76, 60]
      [237, 38, 71]
    ]
    @shapes = []
    @expectResize = -1
    @selected = -1

  initialize: ->
    @canvas = document.getElementById('canvas')
    @ctx = @canvas.getContext('2d')
    ghostcanvas = document.createElement('canvas')
    ghostcanvas.width = @width
    ghostcanvas.height = @height
    @gctx = ghostcanvas.getContext('2d')

    # setup shapes
    i = 0
    for setName, set of @sets
      [r, g, b] = @colours[i % @colours.length]
      a = 0.75
      fillStyle = "rgba(#{r},#{g},#{b},#{a})"
      @shapes[i] = new Shape(@, setName, set, fillStyle)
      i++

    draw = @draw.bind @
    setInterval draw, 20
    @canvasValid = false

    @canvas.onmousedown = @mouseDown.bind @
    @canvas.onmouseup = @mouseUp.bind @
    @canvas.ondblclick = @mouseDblClick.bind @
    @canvas.onmousemove = @mouseMove.bind @

  sX: (x) ->
    r = @padding + (x - @min) / (@max - @min) * (@width - 2 * @padding)

  rX: (r) ->
    x = (r - @padding) * ((@max - @min) / (@width - 2 * @padding)) + @min

  sY: (y) ->
    r = @padding + (@height - 2 * @padding) * (1 - y)

  mouseDown: (e) ->
    [x, y] = @getMouse(e)
    i = 0

    if @expectResize > -1
      @isDrag = true
      return

    for shape in @shapes
      @gctx.clearRect(0, 0, @width, @height)
      @shapes[i].drawShape(@gctx)
      imageData = @gctx.getImageData(x, y, 1, 1)
      if imageData.data[3] > 0
        @selected = i
        shape.selected = true
        @canvasValid = false
      else
        shape.selected = false
      i++

  mouseUp: (e) ->
    if @isDrag
      if shape = @shapes[@selected]
        @onChange(shape.name, shape.set)
    @isDrag = false
    @expectResize = -1

  mouseDblClick: (e) ->
    for shape in @shapes
      if shape.selected
        # convert the shape triangle <-> trapezoid
        if shape.set.length == 3
          newPoint = (shape.set[2] + shape.set[1]) / 2
          shape.set[3] = shape.set[2]
          shape.set[2] = newPoint
          @canvasValid = false
        else if shape.set.length == 4
          shape.set[2] = shape.set[3]
          shape.set.splice 3
          @canvasValid = false

  mouseMove: (e) ->
    [x, y] = @getMouse(e)
    if @isDrag
      if shape = @shapes[@selected]
        oldX = shape.dragHandles[@expectResize].x
        shape.set[@expectResize] = @rX(x)
        @canvasValid = false

    else
      @canvas.style.cursor = 'auto'
      if shape = @shapes[@selected]
        i = 0
        for handle in shape.dragHandles
          if x >= handle.x and x <= handle.x + 6 and y >= handle.y and y <= handle.y + 6
            @canvas.style.cursor = 'ew-resize'
            @expectResize = i
          i++

  getMouse: (e) ->
    rect = @canvas.getBoundingClientRect()

    x = e.offsetX
    y = e.offsetY
    width = rect.right - rect.left
    if @canvas.width != width
      height = rect.bottom - rect.top
      x = x * (@canvas.width / width)
      y = y * (@canvas.height / height)

    [x, y]

  draw: ->
    if @canvasValid
      return

    all = _.flatten _.values @sets
    @min = _.min all
    @max = _.max all

    @ctx.clearRect(0, 0, @width, @height)

    # Horizontal grid
    x = 0.5
    while x < @width
      @ctx.moveTo x, 0
      @ctx.lineTo x, @height
      x += 10

    # Vertical grid
    y = 0.5
    while y < @height
      @ctx.moveTo 0, y
      @ctx.lineTo @width, y
      y += 10

    @ctx.strokeStyle = '#F6F6F3'
    @ctx.stroke()

    # Top and bottom lines
    @ctx.beginPath()
    @ctx.moveTo 0, @padding
    @ctx.lineTo @width, @padding
    @ctx.moveTo 0, @height - @padding
    @ctx.lineTo @width, @height - @padding
    @ctx.strokeStyle = '#45384b'
    @ctx.stroke()

    raw = (@max - @min) / 20.0
    n = 1
    while n < raw
      n *= 10
    if n / raw > 1
      n /= 2
    j = 0
    while j <= @max - @min
      @ctx.beginPath()
      @ctx.moveTo @sX(j), @sY(0.05)
      @ctx.lineTo @sX(j), @sY(-0.05)
      @ctx.strokeStyle = '#45384b'
      @ctx.strokeWidth = 1
      @ctx.stroke()
      @ctx.textAlign = 'center'
      @ctx.textBaseline = 'top'
      @ctx.fillText j.toString(), @sX(j), @sY(-0.06)
      j += n

    i = 0
    selected = null
    for setName, set of @sets
      if i != @selected
        @shapes[i].draw(@ctx)
      i++
    if @selected > -1
      @shapes[@selected].draw(@ctx)

    # canvas is now valid
    @canvasValid = true

exports.Grid = Grid

class Shape
  constructor: (@grid, @name, @set, @fill) ->
    @selected = false

  sX: (x) ->
    r = @grid.sX(x)

  sY: (y) ->
    r = @grid.sY(y)

  setToPoints: (set, min) ->
    switch set.length
      when 2
        if set[0] == min
          # Scale down
          [[set[0], 0], [set[0], 1], [set[1], 0]]
        else
          # Scale up
          [[set[0], 0], [set[1], 1], [set[1], 0]]
      when 3
        # Triangle
        [[set[0], 0], [set[1], 1], [set[2], 0]]
      when 4
        # Triangle
        [[set[0], 0], [set[1], 1], [set[2], 1], [set[3], 0]]

  round: (number, precision) ->
    factor = Math.pow(10, precision)
    temp = number * factor
    roundedTemp = Math.round(temp)
    roundedTemp / factor

  draw: (context) ->
    @dragHandles = []
    @drawShape(context)

    # Draw the name at the center... kind of
    context.fillStyle = '#45384b'
    context.font = '14px sans-serif'
    context.textAlign = 'center'
    context.textBaseline = 'middle'
    cpt = pcentroid @setToPoints @set, @grid.min
    context.fillText @name, @sX(cpt[0]), @sY(cpt[1])

    if @selected
      for cur in @dragHandles
        context.fillStyle = "#CC0000"
        context.fillRect(cur.x, cur.y, 6, 6)
        context.fillStyle = "#000000"
        context.fillText @round(cur.label, 2), cur.x + 10, cur.y - 10
    context.fillStyle = '#45384b'

  addDragHandle: (x, y) ->
    @dragHandles[@dragHandles.length] =
      x: @sX(x) - 3
      y: @sY(y) - 3
      label: x

  drawShape: (context) ->
    context.beginPath()
    context.fillStyle = @fill
    switch @set.length
      when 2
        if @set[0] == @grid.min
          # Slope down
          context.moveTo 0.5, @sY(0)
          context.moveTo 0.5, @sY(1)
          context.lineTo @sX(@set[0]), @sY(1)
          context.lineTo @sX(@set[1]), @sY(0)
          context.lineTo 0.5, @sY(0)
          @addDragHandle(@set[0], 1)
          @addDragHandle(@set[1], 0)
        else
          # Slope up
          context.moveTo @grid.width - 0.5, @sY(0)
          context.lineTo @sX(@set[0]), @sY(0)
          context.lineTo @sX(@set[1]), @sY(1)
          context.lineTo @grid.width - 0.5, @sY(1)
          context.moveTo @grid.width - 0.5, @sY(0)
          @addDragHandle(@set[0], 0)
          @addDragHandle(@set[1], 1)
      when 3
        # Triangle
        context.moveTo @sX(@set[0]), @sY(0)
        context.lineTo @sX(@set[1]), @sY(1)
        context.lineTo @sX(@set[2]), @sY(0)
        context.lineTo @sX(@set[0]), @sY(0)
        @addDragHandle(@set[0], 0)
        @addDragHandle(@set[1], 1)
        @addDragHandle(@set[2], 0)
      when 4
        # Trapezoid
        context.moveTo @sX(@set[0]), @sY(0)
        context.lineTo @sX(@set[1]), @sY(1)
        context.lineTo @sX(@set[2]), @sY(1)
        context.lineTo @sX(@set[3]), @sY(0)
        context.lineTo @sX(@set[0]), @sY(0)
        @addDragHandle(@set[0], 0)
        @addDragHandle(@set[1], 1)
        @addDragHandle(@set[2], 1)
        @addDragHandle(@set[3], 0)
    context.strokeStyle = '#45384b'
    context.stroke()
    context.fill()

exports.Shape = Shape
