# webserver.coffee
# Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

http = require 'http'
https = require 'https'
url = require 'url'
path = require 'path'
assert = require 'assert'
os = require 'os'

_ = require 'lodash'
async = require 'async'
express = require 'express'
bodyParser = require 'body-parser'
favicon = require 'serve-favicon'
db = require 'databank'
Logger = require 'bunyan'
uuid = require 'node-uuid'
session = require 'express-session'
APIClient = require 'fuzzy.ai'
validator = require 'validator'
DatabankStore = require('connect-databank')(session)
request = require 'request'
{WebClient} = require '@fuzzy-ai/web'
LRU = require 'lru-cache'

App = require './server'

AuthClient = require './authclient'
PlanClient = require './planclient'
PaymentsClient = require './paymentsclient'
StatsClient = require './statsclient'
TemplatesClient = require './templatesclient'
HTTPError = require './httperror'
version = require './version'

Databank = db.Databank
DatabankObject = db.DatabankObject

defaults =
  port: 80
  hostname: "localhost"
  address: "0.0.0.0"
  key: null
  cert: null
  driver: "memory"
  params: {}
  logfile: null
  loglevel: "info"
  authServer: "https://auth.fuzzy.ai"
  authKey: "moonbauersuckelsechive"
  sessionSecret: "casey perle gauze cy file"
  apiServer: "https://api.fuzzy.ai"
  apiKey: "edwin-house-um-wife-usn"
  statsServer: "https://stats.fuzzy.ai"
  planServer: "https://plans.fuzzy.ai"
  planKey: "foobar"
  paymentsServer: "https://payments.fuzzy.ai"
  paymentsKey: ""
  templatesServer: "https://templates.fuzzy.ai"
  templatesKey: ""
  caCert: null
  cleanup: 3600000
  apiClientCacheSize: 50
  sessionMaxAge: 30 * 24 * 60 * 60 * 1000 # 30 days
  sessionSecure: false

isJSONType = (type) ->
  expected = "application/json"
  type.substr(0, expected.length) == expected

class WebServer

  constructor: (config) ->

    webserver = @

    setupExpress = (cfg) ->

      makeURL = (rel) ->
        def = (cfg.key? and cfg.port == 443) or (!cfg.key? and cfg.port == 80)
        if def
          host = cfg.hostname
        else
          host = "#{cfg.hostname}:#{cfg.port}"

        props =
          protocol: if cfg.key? then "https:" else "http:"
          host: host
          pathname: rel

        url.format props

      sendToSlack = (message, icon, attachment, callback) ->
        if !callback?
          callback = attachment
          attachment = null
        if !config.slackHook?
          callback null
        else
          options =
            url: config.slackHook
            headers:
              "Content-Type": "application/json"
            json:
              text: message
              username: "home"
              icon_emoji: icon
          # Attachment text
          if attachment?
            options.json.attachments = [{text: attachment}]
          request.post options, (err, response, body) ->
            if err
              callback err
            else
              callback null

      extendedMessage = (err) ->
        ct = err.headers['content-type']
        if ct? and isJSONType(ct)
          JSON.parse(err.body).message
        else
          null

      reportIt = (client, name) ->
        client.on 'error', (err) ->
          slackMsg = "#{err.name}: #{err.message} from #{name} client"
          extraText = extendedMessage err
          sendToSlack slackMsg, ":warning:", extraText, (err) ->
            if err
              req.log.warn {err: err}, "Error posting to Slack"
          exp.log.warn {err: err}, "Error in #{name} client request"

      setupLogger = ->
        logParams =
          serializers:
            req: Logger.stdSerializers.req
            res: Logger.stdSerializers.res
            err: Logger.stdSerializers.err
          level: config.loglevel

        if cfg.logfile
          logParams.streams = [{path: config.logfile}]
        else
          logParams.streams = [{stream: process.stdout}]

        logParams.name = "web"

        log = new Logger logParams

        log.debug "Initializing"

        log

      requestLogger = (req, res, next) ->
        start = Date.now()
        req.id = uuid.v4()
        weblog = req.app.log.child
          "req_id": req.id
          "url": req.originalUrl
          method: req.method
          component: "web"
        end = res.end
        req.log = weblog
        res.end = (chunk, encoding) ->
          res.end = end
          res.end(chunk, encoding)
          rec = {req: req, res: res, elapsed: Date.now() - start}
          weblog.info(rec, "Completed request.")
          next()
        next()

      sessionUser = (req, res, next) ->
        if req.session?.user
          uprops = _.omit(req.session?.user, ["password", "passwordHash"])
          res.locals.user = uprops
          req.user = uprops
        else
          res.locals.user = null
          req.user = null
        next()

      # API client uses the session token, so we create a new one each
      # request.

      reqAPIClient = (req, res, next) ->
        token = req.session?.token
        if token?
          client = req.app.apiClients.get token
          if !client?
            client = new APIClient
              key: token
              root: req.app.config.apiServer
              webClient: req.app.webClient
            reportIt client, 'api'
            req.app.apiClients.set token, client
          req.apiClient = client
        next()

      exp = express()
      exp.config = cfg
      exp.log = setupLogger()

      # Hide the "X-Powered-By" header Express uses

      exp.disable 'x-powered-by'

      # Redirect if we're HTTPS and getting hit by HTTP

      if cfg.redirectToHttps
        exp.use (req, res, next) ->
          if req.headers['x-forwarded-proto'] == "http"
            res.redirect(301, "https://#{req.hostname}#{req.url}")
          else
            next()

      # Add our own "Server" header

      exp.use (req, res, next) ->
        res.set('Server', "fuzzy.ai/#{version}")
        next()

      # Short-circuit if it's a Google Health Check
      # No session or other setup

      exp.use (req, res, next) ->
        if req.headers["user-agent"]?.match("^GoogleHC")
          res.set("Content-Type", "text/plain")
          res.send("OK")
        else
          next()

      # We keep a cache of API clients.

      exp.apiClients = new LRU max: cfg.apiClientCacheSize

      # We use a custom web client with persistent connections enabled

      exp.webClient = new WebClient timeout: cfg.webClientTimeout

      # We keep these clients available for requests

      exp.auth = new AuthClient(cfg.authServer, cfg.authKey, cfg.caCert)
      exp.stats = new StatsClient
        root: cfg.statsServer
        key: cfg.statsKey
        webClient: exp.webClient
      reportIt exp.stats, "stats"
      exp.plan = new PlanClient
        root: cfg.planServer
        key: cfg.planKey
        webClient: exp.webClient
      reportIt exp.plan, "plan"
      exp.payments = new PaymentsClient
        root: cfg.paymentsServer
        key: cfg.paymentsKey
        webClient: exp.webClient
      reportIt exp.payments, "payments"
      exp.templates = new TemplatesClient
        root: cfg.templatesServer
        key: cfg.templatesKey
        webClient: exp.webClient
      reportIt exp.templates, "templates"

      # Develpment mode tweaks
      if process.env.NODE_ENV == 'development'
        webpack = require('webpack')
        webpackConfig = require '../webpack.config'
        webpackConfig.entry.unshift('webpack-hot-middleware/client?reload=true')
        webpackConfig.plugins.push(new webpack.HotModuleReplacementPlugin())
        compiler = webpack(webpackConfig)

        exp.use require('webpack-dev-middleware')(compiler, {
          noInfo: true,
          publicPath: webpackConfig.output.publicPath
        })
        exp.use(require('webpack-hot-middleware')(compiler))

      exp.use express.static path.join(__dirname, '..', 'public')

      # XXX: this should get passed down but YOLO

      store = new DatabankStore webserver.db, exp.log, cfg.cleanup

      exp.use session
        secret: cfg.sessionSecret
        store: store
        resave: true
        saveUninitialized: true
        cookie:
          maxAge: cfg.sessionMaxAge
          secure: cfg.sessionSecure

      exp.use sessionUser
      exp.use reqAPIClient
      exp.use bodyParser.urlencoded({extended: true})
      exp.use bodyParser.json()
      fiPath = path.join(__dirname, '..', 'public', 'images', 'favicon.ico')
      exp.use favicon fiPath
      exp.use requestLogger

      exp.param 'agentID', (req, res, next, id) ->
        if !req.session?.user?
          next()
        else
          req.apiClient.getAgent id, (err, agent) ->
            if err
              if isJSONType(err.headers?['content-type'])
                data = JSON.parse err.body
                next new HTTPError(data.message, err.statusCode)
              else
                next err
            else
              req.agent = agent
              next()

      exp.param 'stripeID', (req, res, next, id) ->
        req.app.plan.getPlan id, (err, plan) ->
          if err
            next err
          else
            req.plan = plan
            next()

      exp.param 'reqID', (req, res, next, id) ->
        req.app.stats.getLog id, (err, log) ->
          if err
            next err
          else
            req.evaluationLog = log
            next()

      exp.param 'invoiceID', (req, res, next, id) ->
        req.app.payments.getInvoice id, (err, invoice) ->
          if err
            next err
          else
            req.invoice = invoice
            next()

      withAgents = (req, res, next) ->
        if !req.session?.user?
          next()
        else
          req.apiClient.getAgents (err, agents) ->
            if err
              next err
            else
              _.each agents, (agent) ->
                agent.name = "(Unnamed)" if !agent.name?
              req.agents = agents
              next()

      userRequired = (req, res, next) ->
        if req.session?.user?
          next()
        else
          if req.headers.accept == 'application/json'
            next new HTTPError('Unauthorized', 401)
          else
            req.session.afterLogin = req.url
            res.redirect(303, "/login")

      # Handle invitation request requests

      exp.post "/request", (req, res, next) ->
        email = req.body.email
        if !email?
          next new Error("No email address provided.")
        else
          req.app.auth.request email, (err) ->
            if err
              if err.statusCode == 409
                next new Error("You have already requested an invitation.")
              else
                next err
            else
              res.json
                status: "OK"

      exp.post "/signup", (req, res, next) ->
        email = req.body.email
        if !email? or email.length == 0
          return next new HTTPError("Email address required.", 400)

        password = req.body.password
        if !password? or password.length == 0
          return next new HTTPError("Password required.", 400)

        if password.length < 8
          return next new HTTPError("Password must be at least 8 characters.", 400)

        confirm = req.body.confirm

        if !confirm? or confirm != password
          return next new HTTPError("Password confirmation must match.", 400)

        tos = req.body.tos

        if !tos?
          return next new HTTPError("You must agree to the terms of service.", 400)

        code = req.body.code
        subscribe = req.body.subscribe

        logprops =
          email: email
          password: Array(password.length).join '*'
          code: code
          subscribe: subscribe

        req.log.info logprops, "Registering user"

        req.app.auth.register email, password, code, subscribe, (err, results) ->
          if err
            next err
          else
            res.json
              status: "OK"

      exp.get "/confirm/:code", (req, res, next) ->
        code = req.params.code
        req.log.info {code: code}, "Confirming registration"
        req.app.auth.confirm code, (err, results) ->
          if err
            if err.statusCode == 409 # Already confirmed
              if req.user?
                res.redirect(303, '/')
              else
                res.redirect(303, '/login')
            else
              next err
          else
            req.log.info {email: results?.user?.email}, "Registration confirmed"
            req.session.token = results.token
            res.locals.user = req.session.user = results.user
            req.session.needTour = true
            res.redirect(303, '/confirmed')

      # Handle the login page
      exp.post "/login", (req, res, next) ->
        email = req.body.email
        if !email? or email.length == 0
          return next new HTTPError("Email address required.", 400)

        password = req.body.password
        if !password? or password.length == 0
          return next new HTTPError("Password required.", 400)

        req.log.info {email: email, password: Array(password.length).join '*'}, "Logging in user"

        req.app.auth.login email, password, (err, results) ->
          if err
            next err
          else
            req.session.token = results.token
            res.locals.user = req.session.user = results.user
            url = req.session.afterLogin
            if url?
              delete req.session.afterLogin
            else
              url = '/'

            if req.headers.accept == 'application/json'
              res.json
                status: 'OK'
                user: results.user
                url: url
            else
              res.redirect(303, url)


      exp.post "/logout", (req, res, next) ->
        req.log.info {email: req.session.user.email, token: req.session.token}, "Logging out user"
        req.app.auth.logout req.session.token, (err, results) ->
          if err
            req.log.info "Error logging out user with auth service"
          delete req.session.token
          delete req.session.user
          res.redirect(303,  '/')

      exp.get "/reset-confirm/:code", (req, res, next) ->
        {code} = req.params
        req.session.resetCode = code
        res.redirect(303, "/reset-password")

      # Handle the password reset page
      exp.post "/reset", (req, res, next) ->
        {email} = req.body
        if !validator.isEmail(email)
          return next new Error("Invalid email: #{email}")
        req.app.auth.requestReset email, (err) ->
          if err
            req.log.error
              err: err
              message: "Error requesting password reset for #{email}"
            next new Error("Unable to request a password reset for #{email}.")
          else
            if req.headers.accept == 'application/json'
              res.json
                status: 'OK'
            else
              res.redirect(303, "/reset-requested")

      exp.post "/change-password", (req, res, next) ->

        code = req.session.resetCode

        if !code
          return next new Error("Need a reset code")

        {password, confirmation} = req.body

        if !password? or password.length == 0
          return next new HTTPError("Password required.", 400)

        if password.length < 8
          return next new HTTPError("Password must be at least 8 characters.", 400)

        req.app.auth.reset code, password, (err, results) ->
          if err
            next err
          else
            delete req.session.resetCode
            req.session.token = results.token
            res.locals.user = req.session.user = results.user
            res.json
              status: 'OK'

      exp.post "/resend-confirmation", (req, res, next) ->
        {email} = req.body
        req.app.auth.resendConfirmation email, (err, results) ->
          if err
            next err
          else
            res.json
              status: 'OK'

      # User data calls
      exp.put '/data/user', userRequired, (req, res, next) ->
        req.app.auth.updateUser req.body, req.user.id, (err, results) ->
          if err
            next err
          else
            req.session.user = results.user
            res.json results

      exp.post '/data/change-password', userRequired, (req, res, next) ->
        req.app.auth.changePassword req.user.id, req.body, (err, results) ->
          if err
            next err
          else
            res.json results

      # Get all user API tokens
      exp.get '/data/tokens', userRequired, (req, res, next) ->
        req.app.auth.getTokens req.session.token, (err, results) ->
          if err
            next err
          else
            res.json results

      exp.post '/data/tokens', userRequired, (req, res, next) ->
        {name} = req.body
        req.app.auth.newToken name, req.session.token, (err, results) ->
          if err
            next err
          else
            res.json results

      ###
      #  Agent API calls
      ###

      # Get all of a user's agents.
      exp.get "/data/agents", userRequired, withAgents, (req, res, next) ->
        res.json req.agents

      exp.get "/data/agent/:agentID", userRequired, (req, res, next) ->
        res.json req.agent

      # Save an agent.
      exp.put "/data/agent/:agentID", userRequired, (req, res, next) ->
        newAgent = req.body
        req.apiClient.putAgent req.agent.id, newAgent, (err, agent) ->
          if err
            if isJSONType(err.headers?['content-type'])
              data = JSON.parse err.body
              next new HTTPError(data.message, err.statusCode)
            else
              next err
          else
            res.json
              status: 'OK'
              agent: agent

      # Execute a request to an agent, with meta
      exp.post "/data/agent/:agentID", userRequired, (req, res, next) ->
        inputs = req.body

        if !_.every(inputs, (value) -> _.isFinite(value) or _.isNull(value))
          next new HTTPError("All inputs must be numbers", 400)

        # FIXME This will cause problems if agent has an output named 'meta'
        req.apiClient.evaluate req.agent.id, inputs, true, (err, outputs) ->
          if err
            next err
          else
            res.json outputs

      # Create a new agent
      exp.post "/data/agents", userRequired, (req, res, next) ->
        newAgent = req.body
        console.log JSON.stringify newAgent
        req.apiClient.newAgent newAgent, (err, agent) ->
          if err
            if isJSONType(err.headers?['content-type'])
              data = JSON.parse err.body
              next new HTTPError(data.message, err.statusCode)
            else
              next err
          else
            res.json
              status: 'OK'
              agent: agent
              url: "/agents/#{agent.id}"

      # Delete an agent
      exp.delete "/data/agents/:agentID", userRequired, (req, res, next) ->
        req.apiClient.deleteAgent req.agent.id, (err, agent) ->
          if err
            next err
          else
            res.json
              status: 'OK'

      exp.get "/data/templates", userRequired, (req, res, next) ->
        req.app.templates.templates (err, templates) ->
          if err
            next err
          else
            res.json(templates)

      exp.get "/data/stats/user/:year/:month", userRequired, (req, res, next) ->

        try
          year = parseInt(req.params.year, 10)
          month = parseInt(req.params.month, 10)
        catch err
          return next err

        if year < 2014 or year > (new Date()).getUTCFullYear()
          return next new Error("Invalid year #{year}")

        if month < 1 or month > 12
          return next new Error("Invalid month #{month}")

        req.app.stats.getUserStats req.user.id, year, month, (err, stats) ->
          if err
            next err
          else
            res.json stats

      exp.get "/data/stats/agent/:agentID/:year/:month", userRequired, (req, res, next) ->

        try
          year = parseInt(req.params.year, 10)
          month = parseInt(req.params.month, 10)
        catch err
          return next err

        if year < 2014 or year > (new Date()).getUTCFullYear()
          return next new Error("Invalid year #{year}")

        if month < 1 or month > 12
          return next new Error("Invalid month #{month}")

        req.app.stats.getAgentStats req.agent.id, year, month, (err, stats) ->
          if err
            next err
          else
            res.json stats

      exp.get "/data/logs", userRequired, (req, res, next) ->

        try
          offset = if req.query.offset then parseInt(req.query.offset, 10) else 0
          limit = if req.query.limit then parseInt(req.query.limit, 10) else 20
        catch err
          return next err

        req.app.stats.getUserLogs req.user.id, offset, limit, (err, logs) ->
          if err
            next err
          else
            res.json logs

      exp.get "/data/agent/:agentID/logs", userRequired, (req, res, next) ->

        try
          offset = if req.query.offset then parseInt(req.query.offset, 10) else 0
          limit = if req.query.limit then parseInt(req.query.limit, 10) else 20
        catch err
          return next err

        req.app.stats.getAgentLogs req.agent.id, offset, limit, (err, logs) ->
          if err
            next err
          else
            res.json logs

      exp.get '/data/log/:reqID', userRequired, (req, res, next) ->
        log = req.evaluationLog
        async.parallel [
          (callback) ->
            req.apiClient.getAgentVersion log.versionID, callback
          (callback) ->
            req.apiClient.getAgent log.agentID, callback
        ], (err, results) ->
          if err
            next err
          else
            log.version = results[0]
            log.agent = results[1]
            timestamps = [
              log.createdAt
              log.version.createdAt
              log.agent.updatedAt
            ]
            lm = _.max(timestamps)
            res.set "Last-Modified", (new Date(lm)).toUTCString()
            res.json log

      exp.get "/data/plans", (req, res, next) ->
        req.app.plan.plans (err, plans) ->
          if err
            next err
          else
            res.json(plans)

      exp.get "/data/plan/:stripeID", (req, res, next) ->
        res.json req.plan

      exp.post "/data/tutorial", (req, res, next) ->
        data = _.pick req.body, ['agentID', 'step', 'completed']
        req.session.tutorial = _.assign req.session.tutorial, data
        user = req.user
        user.tutorial = req.session.tutorial
        req.app.auth.updateUser user, req.user.id, (err, results) ->
          if err
            next err
          else
            req.session.user = results.user
            res.json results

      exp.post "/upgrade", userRequired, (req, res, next) ->
        data = req.body
        data.email = req.session.user.email
        data.userID = req.user.id
        req.app.payments.setCustomer data, (err, response) ->
          if err
            if isJSONType(err.headers?['content-type'])
              data = JSON.parse err.body
              next new HTTPError(data.message, err.statusCode)
            else
              next err
          else
            req.session.user = response.user
            res.json response

      exp.get "/data/customer", userRequired, (req, res, next) ->
        if not req.user.customerID
          res.json {status: 'OK'}
        else
          req.app.payments.getCustomer req.user.customerID, (err, response) ->
            if err
              next err
            else
              res.json response

      exp.put "/data/customer", userRequired, (req, res, next) ->
        data = req.body
        customerID = req.user.customerID
        req.app.payments.updateCustomer customerID, data, (err, response) ->
          if err
            if isJSONType(err.headers?['content-type'])
              data = JSON.parse err.body
              next new HTTPError(data.message, err.statusCode)
            else
              next err
          else
            if response.user
              req.session.user = response.user
            res.json response

      exp.get "/data/invoices", userRequired, (req, res, next) ->
        if not req.user.customerID
          res.json {status: 'OK'}
        else
          req.app.payments.getInvoices req.user.customerID, (err, response) ->
            if err
              next err
            else
              res.json response

      exp.get "/data/invoices/:invoiceID", userRequired, (req, res, next) ->
        res.json req.invoice

      exp.get '/version', (req, res, next) ->
        res.json {name: 'web', version: version}

      exp.post '/end-tour', (req, res, next) ->
        req.session?.needTour = false
        res.locals.needTour = false
        res.json {message: OK}

      # React Router
      exp.get '*', (req, res, next) ->
        App(req, res, next)

      # Error handler
      exp.use (err, req, res, next) ->

        config = req?.app?.config

        if err.statusCode
          res.statusCode = err.statusCode
        else
          switch err.name
            when "NoSuchThingError"
              res.statusCode = 404
            when "URIError"
              res.statusCode = 400
            when "SyntaxError"
              res.statusCode = 400
            else
              res.statusCode = 500

        if req.log
          req.log.error {err: err}, "Error"

        userId = req.user?.email or "(unauthenticated)"
        method = req.method
        url = req.originalUrl

        reqInfo = "#{method} #{url} by #{userId}"

        # Post 5xx errors to Slack

        if res.statusCode >= 500 and res.statusCode < 600
          hostname = os.hostname()
          messageText = "#{hostname} #{err.name}: #{err.message}\n#{reqInfo}"
          sendToSlack messageText, ":bomb:", (err) ->
            if err
              req.log.error {err: err}, "Error posting to Slack"

        if !res.headerSent
          # XXX: route through error component
          res.json {status: 'error', message: err.message}

      exp

    startNetwork = (callback) =>

      if @config.key
        options =
          key: @config.key
          cert: @config.cert
        @srv = https.createServer(options, @express)
      else
        @srv = http.createServer(@express)

      @srv.once 'error', (err) ->
        callback err
      @srv.once 'listening', ->
        callback null
      address = @config.address or @config.hostname
      @srv.listen @config.port, address

    stopNetwork = (callback) =>

      if !@srv?
        callback null
      else
        @srv.once 'close', ->
          callback null
        @srv.once 'error', (err) ->
          callback err
        @srv.close()

    startDatabase = (callback) =>
      if @db?
        @db.connect @config.params, callback
      else
        callback new Error("db property not set for webserver")

    stopDatabase = (callback) =>
      if @db?
        @db.disconnect callback
      else
        callback null

    @config = _.defaults config, defaults

    if @config.key and @config.port == defaults.port
      @config.port = 443

    # Databank is not yet connected!

    @db = Databank.get @config.driver, @config.params

    @express = setupExpress @config

    @start = (callback) =>

      async.waterfall [
        (callback) ->
          startDatabase callback
        (callback) ->
          startNetwork callback
        (callback) =>
          apiClient = new APIClient(@config.apiKey, @config.apiServer)
          apiClient.on 'error', (err) =>
            @express.log.warn
              msg: "Error getting API server version; continuing"
              err: err
          apiClient.apiVersion (err, versionData) =>
            if !err
              # Any errors will be logged by above handler
              @express.log.info
                msg: "API server successfully contacted"
                versionData: versionData
            callback null
      ], callback

    @stop = (callback) ->

      # If we have persistent connections, stop them

      if @express.webClient
        @express.webClient.stop()

      async.waterfall [
        (callback) ->
          stopNetwork callback
        (callback) ->
          stopDatabase callback
      ], callback

module.exports = WebServer
