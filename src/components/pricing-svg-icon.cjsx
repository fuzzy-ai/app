React = require 'react'
{ Link } = require 'react-router'

module.exports = React.createClass

  displayName: "pricing icon"

  # coffeelint: disable=max_line_length
  render: ->
    <svg xmlns="http://www.w3.org/2000/svg" id="pricing-simple-icon" className="home-svg-icon" viewBox="0 0 501.16 194.65">

      <path id="tag" d="M243.27 175.75l-74.23-42.17L232.3 22.24 293.51.9l13 63.5-63.24 111.35z" className="cls-1"/>
      <path id="second-tag" d="M307.71 82.11l-63.26 111.38-74.23-42.17" className="cls-1"/>
      <circle id="circle" cx="278.4" cy="28.51" r="9.15" className="cls-1" transform="rotate(-60.4 277.81 28.508)"/>
      <path id="dollar-sign" d="M246.13 101.21l-3.9 6.84c-2.61 4.59-6.14 5.56-10.62 3.14l-2.25 4-4.27-2.43 2.25-4c-4.43-2.66-5.31-6.15-2.72-10.75l4.44-7.8 7.16 4.08-4 7.08a1.34 1.34 0 0 0 .49 2.12l.59.34a1.34 1.34 0 0 0 2.1-.59l2.59-4.54a3 3 0 0 0 .09-3.13l-3.54-8a7.17 7.17 0 0 1 .35-7.58l3.29-5.77c2.59-4.54 6.09-5.59 10.58-3.18l2.13-3.74 4.27 2.43-2.13 3.74c4.37 2.63 5.25 6.17 2.67 10.72l-3.8 6.68-7.16-4.08 3.41-6a1.34 1.34 0 0 0-.59-2.1l-.48-.27a1.34 1.34 0 0 0-2.1.59l-2 3.46a3 3 0 0 0-.09 3.13l3.54 8c1.38 3 1.25 4.83-.36 7.61z" className="cls-1"/>
    </svg>
