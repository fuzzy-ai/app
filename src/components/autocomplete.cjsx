React = require 'react'
Radium = require 'radium'

ListItem = React.createClass
  displayName: 'ListItem'

  handleClick: (e) ->
    e.preventDefault()
    @props.handleClick(@props.value)

  getDefaultProps: ->
    arrowStyle:
      display: 'block'
      position: 'absolute'
      top: '-1px'
      right: '50%'
      left: '50%'
      width: '0px'
      height: '0px'


    listStyle:
      padding: '5px 0'
      margin: '0px 0px 1px 0px'
      textAlign: 'center'
      fontWeight: 'normal'
      background: '#fffef7'
      display: 'block'
      fontSize: '12px'
      color: '#85649c'
      ':hover': {
        boxShadow: 'inset 0 0 0 100px #FAFBF3'
        color: '#473d4e'
        transitionDuration: '.3s'
        transitionTimingFunction: 'ease-in'
      }

  # coffeelint: disable=max_line_length
  render: ->
    <li onClick={@handleClick} style={@props.listStyle} >
        <em style={@props.arrowStyle}></em>
        {@props.value}
    </li>

ListItem = Radium(ListItem)

Autocomplete = React.createClass
  displayName: 'Autocomplete'


  getDefaultProps: ->

    wrapperStyle:
      position: 'relative'
      zIndex: 10000
    ulStyle:
      background: '#dce0e4'
      padding: '0px'
      border: '1px solid #dce0e4'
      boxShadow: '0px 4px 10px -10px rgba(55,55,55, 0.5)'
      fontSize: '12px'
      position: 'absolute'
      minHeight: '100%'
      width: '100%'
      top: '100%'
      zIndex: '999'


  getInitialState: ->
    showList: false
    highlightedIndex: null

  renderItems: ->
    handleClick = @handleClick
    showItems = @searchItems(@props.items, @props.value)

    if not showItems.length
      return

    children = []
    showItems.map (item, i) ->
      children.push <ListItem key={i} handleClick={handleClick} value={item}/>

    <div className="autocomplete-wrapper-inner" style={@props.wrapperStyle}>
      <ul style={@props.ulStyle}>
        {children}
      </ul>
    </div>

  searchItems: (items, value) ->
    if not items
      return []

    searchTerm = new RegExp(value, 'i')
    results = []
    items.map (item, i) ->
      results.push item if searchTerm.exec(item)
    results

  handleFocus: (e) ->
    @setState showList: true

  handleBlur: (e) ->
    if @props.onBlur
      options = @searchItems(@props.items, e.target.value)
      @props.onBlur(e.target.value, options)

  handleClick: (value) ->
    @props.onChange(value)
    @setState showList: false

  handleChange: (e) ->
    @props.onChange(e.target.value)

  render: ->
    <div className="autocomplete-wrapper">
      <input
        type="text"
        role="combobox"
        onFocus={@handleFocus}
        onBlur={@handleBlur}
        onChange={@handleChange}
        value={@props.value}
        placeholder={@props.placeholder}
        required
      />
      {@state.showList && @renderItems()}
    </div>

styles = {}

module.exports = Radium(Autocomplete)
