React = require 'react'

module.exports = React.createClass
  displayName: 'AccountInfoSettings'

  handleSubmit: (e) ->
    e.preventDefault()
    {submitHandler} = @props

    data =
      email: @refs.email.value
    submitHandler(data)

  # coffeelint: disable=max_line_length
  render: ->
    <div className="module Profile-module">
      <h2>Account Information</h2>
      <form action="#" onSubmit={@handleSubmit} className="user-forms">
        <div className="form-group input-wrap">
          <label className="upper spaced-medium" htmlFor="password">Your Email</label>
          <input className="form-control" defaultValue={@props.user?.email} ref="email" type="text" />
        </div>
        <button className="btn">Save</button>
      </form>
    </div>
