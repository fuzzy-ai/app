React = require 'react'
{ Link } = require 'react-router'
{ connect } = require 'react-redux'

{ requestInvite, reset } = require '../actions/user'

Request = React.createClass
  displayName: "Request an invitation"

  componentDidMount: ->
    { dispatch } = @props
    dispatch(reset())

  handleSubmit: (e) ->
    e.preventDefault()

    email = @refs.email.value

    {dispatch} = @props
    dispatch(requestInvite(
      email: email
    ))

  # coffeelint: disable=max_line_length
  render: ->
    { inProgress, error } = @props.user
    <div className="row">
      <div className="page">
        <p id="form-parent"></p>
        <div className="form-wrapper">
          <div className="form_text">
            <h1 className="page_title">Request an invitation</h1>
            <p><Link to="/login">fuzzy.ai is currently available by invitation only.</Link></p>
            {if error
              <p className="error_message">{ error }</p>
            }
          </div>
          <p id="form-parent"></p>
          <form action="/request" id="request" method="post" className="user-forms" name="request" role="form" onSubmit={@handleSubmit}>
            <fieldset>
              <div className="form-group input-wrap">
                <label htmlFor="email" className="upper">Email address</label>
                <input className="form-control" id="email" name="email" ref="email" placeholder="Enter email" required="required" type="email" />
                <label className="error" htmlFor="email"></label>
                <br />
                <Link to="/signup" className="form_user_request form_user_request__invite">I already have an invitation code.</Link>
              </div>
              { if inProgress
                <button className="btn btn-default" disabled="disabled">Working...</button>
              else
                <button className="btn btn__regular" type="submit">Request</button>
              }
            </fieldset>
          </form>
        </div>
      </div>
    </div>


mapStateToProps = (state) ->
  return {
    user: state.user
  }

module.exports = connect(mapStateToProps)(Request)
