_ = require 'lodash'
React = require 'react'
Link = require 'react-router/lib/Link'
{ connect } = require 'react-redux'

{ testAgent, testAgentReset } = require '../actions/agent'

TestResults = require './test-results'
ErrorBar = require './error-bar'

FuzzySetMembership = require './fuzzy-set-membership'
FuzzyOutput = require './fuzzy-output'

{sigfigs, fmt} = require '../sigfigs'

AgentsTest = React.createClass
  displayName: 'Agent Test'

  getInitialState: ->
    inputs: {}
    agent: null
    output: null

  componentDidMount: ->
    { dispatch, agent} = @props
    dispatch testAgentReset(agent)

  handleRunTest: (e) ->

    e.preventDefault()

    { dispatch, agent } = @props

    inputs = {}
    for name, sets of agent?.inputs
      if @_isActive(name, agent?.parsed_rules)
        inputs[name] = @refs[name].value

    toSend = _.mapValues inputs, (value) ->
      if !value? or value.length == 0
        null
      else
        parsed = parseFloat value
        if isNaN(parsed)
          null
        else
          parsed

    toSend = _.omitBy(toSend, _.isNil)
    if not _.isEmpty(toSend)
      @setState inputs: toSend
      dispatch(testAgent(agent, toSend))

  _findDeep: (obj, value) ->
    if not obj
      return false
    if obj.input == value
      return true
    if obj.dimension == value
      return true
    if arguments.callee(obj.antecedent, value)
      return true
    if arguments.callee(obj.consequent, value)
      return true

  _isActive: (input, rules) ->
    findDeep = @_findDeep
    return _.find rules, (o) ->
      return findDeep(o, input)

  # coffeelint: disable=max_line_length
  render: ->
    { agent, outputs, meta, timing, error } = @props
    { inputs } = @state
    isActive = @_isActive

    sig = sigfigs inputs

    <div className="agent-test-wrapper">
      <div className="agent-test">
        <form className="agent-test-form" method="post" onSubmit={@handleRunTest}>
          { for name, sets of agent?.inputs
            if isActive(name, agent?.parsed_rules)
              <div key={"form-fields-" + _.kebabCase(name)} id={"form-fields-" + _.kebabCase(name)} className="input-wrap">
                <label id={"label-" + _.kebabCase(name)} htmlFor={"input-" + _.kebabCase(name)}>{name}</label>
                <input id={"input-" + _.kebabCase(name)} ref={name} name={name} defaultValue={inputs?[name]} className="num" type="number" placeholder="Enter A Value" />
              </div>
          }
          <button type="submit" name="submit" value="Test" className="btn btn__regular btn-blue"> Test your agent </button>
        </form>

        <div className="calculated-test-results-outputs-wrapper">
          <span className="pointer-arrow"></span>

          { if meta?
            <TestResults inputs={inputs} outputs={outputs} timing={timing} {...meta} />
          }

        </div>
      </div>

      { if meta?
        <div className="agent-logs ">
          <h2 className="fwb">Inputs</h2>

          <div className="agent-logs__data ">
            { _.map inputs, (value, name) ->
              if isActive(name, agent?.parsed_rules)
                <div className="agent-logs-content agent-logs__input" key="#{_.kebabCase(name)}">
                  <div className="agent-logs-content__text">
                    <h3>{name}</h3>
                    <ul>
                      { _.map meta.fuzzified[name], (membership, set) ->
                        if membership > 0
                          <li key="#{set}">{set}: {fmt(membership, sig)}</li>
                      }
                    </ul>
                  </div>
                  <div className="agent-logs-content__chart">
                  <span className="pointer-arrow"></span>
                    <FuzzySetMembership
                      width="800"
                      height="240"
                      padding="30"
                      value={value}
                      sets={agent.inputs[name]}
                      memberships={meta.fuzzified[name]}/>
                  </div>
                </div>
            }
          </div>



          <div className="log-rules">
            <h2 className="fwb">Rules</h2>
            <div className="log-rules-inner">
              {
                _.map meta.rules, (rule, i) ->
                  <p key={i}>{agent.rules[i]}</p>
              }
            </div>
          </div>

          <div className="agent-logs-outputs ">
            <h2 className="fwb">Outputs</h2>

            <div className="agent-logs__data ">

              {_.map _.omit(outputs, "_evaluation_id"), (value, name) ->
                <div className="agent-logs-content test-details__output" key={_.kebabCase(name)}>
                  <div className="agent-logs-content__text">
                    <h3>{name}</h3>
                      <ul>
                        { _.map meta.inferred[name], (membership, set) ->
                          if membership > 0
                            <li key="#{set}">{set}: {fmt(membership, sig)}</li>
                        }
                      </ul>
                  </div>
                  <div className="agent-logs-content__chart">
                    <span className="pointer-arrow"></span>
                    <FuzzyOutput
                      width={800}
                      height={240}
                      padding={20}
                      clipped={meta.clipped[name]}
                      combined={meta.combined[name]}
                      centroid={meta.centroid[name]} />
                  </div>
                </div>
              }
            </div>
          </div>
        </div>
      }
    </div>

mapStateToProps = (state) ->
  agent: state.agent.current
  error: state.agent.error
  outputs: state.agent.outputs
  meta: state.agent.meta
  timing: state.agent.timing

module.exports = connect(mapStateToProps)(AgentsTest)
