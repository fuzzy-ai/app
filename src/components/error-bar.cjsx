React = require 'react'
{ DeleteCross } = require './icons'
ErrorBar = React.createClass
  displayName: "Error Bar"

  getInitialState: ->
    err: @props.err
    errorBarOpen: true


  toggleErrorBar: ->
    @setState errorBarOpen: false





  # coffeelint: disable=max_line_length
  render: ->
    {err} = @props
    {errorBarOpen} = @state

    if errorBarOpen
      errorBarClass = "error-bar"
      handleErrorBarToggle  = @toggleErrorBar
    else
      errorBarClass = "error-bar error-bar-is-closed"
      handleErrorBarToggle  = @toggleErrorBar


    <div className={errorBarClass} data-have-error={err?}>
      {if err
        <div className="error">
        <span className="close-error-bar" onClick={handleErrorBarToggle}><DeleteCross /></span>
          {err.message}
        </div>
      }
    </div>

module.exports = ErrorBar
