React = require 'react'
{ Link } = require 'react-router'
{ connect } = require 'react-redux'

{ fetchCustomer, updateCustomer, customerError,
  fetchInvoices} = require '../actions/payments'
{ fetchPlans } = require '../actions/plans'
{ fetchStats } = require '../actions/stats'
{ fetchTokens, newToken } = require '../actions/tokens'
{ updateUser, changePassword } = require '../actions/user'

AccountInfo = require './settings-account-info'
PlanInfo = require './settings-plan-info'
ApiKeys = require './settings-api-keys'
ChangePassword = require './settings-change-password'
BillingInfo = require './settings-billing-info'
CreditCard = require './settings-credit-card'
ProgressBar = require './progress-bar'
ErrorBar = require './error-bar'

Settings = React.createClass

  displayName: "Settings"

  componentDidMount: ->
    { dispatch } = @props
    dispatch(fetchPlans())
    dispatch(fetchTokens())
    dispatch(fetchCustomer())
    dispatch(fetchInvoices())

    # getStats
    now = new Date()
    year = now.getUTCFullYear()
    month = now.getUTCMonth() + 1
    dispatch(fetchStats(year, month))

  handleUserSubmit: (data) ->
    {dispatch} = @props
    dispatch(updateUser(data))

  handleNewToken: (data) ->
    {dispatch} = @props
    dispatch(newToken(data))

  handleChangePassword: (data) ->
    {dispatch} = @props
    dispatch(changePassword(data))

  handleUpdateCreditCard: (target) ->
    { dispatch } = @props
    Stripe.card.createToken target, (status, response) ->
      if response.error
        dispatch(customerError(response.error.message))
      else
        dispatch(updateCustomer(source: response.id))

  # coffeelint: disable=max_line_length
  render: ->
    { inProgress, error } = @props
    err = {message: error}
    <div className="site">
      <div className="row">
        {if error?
          <ErrorBar err={err} />
        }
      <ProgressBar inProgress={inProgress} />
      <div className="page  module-wrap">
          <div className="user-settings">
            <div className="profile">
              <AccountInfo user={@props.user.account} submitHandler={@handleUserSubmit} />
              <PlanInfo user={@props.user.account} plans={@props.plans} stats={@props.stats} />
              <ApiKeys user={@props.user.account} tokens={@props.tokens} handleNewToken={@handleNewToken} />
              <ChangePassword user={@props.user} submitHandler={@handleChangePassword} />
            </div>
          </div>

          <div className="billing-setting">
            <BillingInfo user={@props.user.account} plans={@props.plans} invoices={@props.payments.invoices} />
            <CreditCard customer={@props.payments.customer} error={@props.payments.error} submitHandler={@handleUpdateCreditCard} />
          </div>
        </div>
      </div>
    </div>

mapStateToProps = (state) ->
  user: state.user
  plans: state.plans.items
  stats: state.stats
  tokens: state.tokens.items
  payments: state.payments
  error: state.plans.error || state.user.error || state.payments.error || state.tokens.error
  inProgress: state.user.inProgress || state.plans.inProgress || state.payments.inProgress || state.tokens.inProgress

module.exports = connect(mapStateToProps)(Settings)
