React = require 'react'
{ Link } = require 'react-router'
{ connect } = require 'react-redux'

{ resetRequest } = require '../actions/user'

ProgressBar = require './progress-bar'
ErrorBar = require './error-bar'

Reset = React.createClass
  displayName: "Reset password"

  handleSubmit: (e) ->
    e.preventDefault()

    { dispatch } = @props

    email = @refs.email.value.trim()

    dispatch resetRequest({email: email})

  # coffeelint: disable=max_line_length
  render: ->
    { inProgress, error } = @props.user
    <div className="row">
      <div className="page">
        {if error?
          <ErrorBar err={error} />
        }
        <ProgressBar inProgress={inProgress} />
        <div className="form-wrapper">
          <div className="form_text">
            <h1>Password reset</h1>
            <p>Forgot your password? Give us your email address and we&apos;ll send
            you a link to let you reset it.</p>
          </div>
          <form action="#" method="post" className="user-forms" role="form" onSubmit={@handleSubmit}>
            <div className="form-group input-wrap">
              <label htmlFor="email">Email address</label>
              <input className="form-control" id="email" name="email"
                placeholder="Enter email" type="email" ref="email" />
            </div>
              <button className="btn btn-default" type="submit">Reset</button>
          </form>
        </div>
      </div>
    </div>


mapStateToProps = (state) ->
  return {
    user: state.user
  }

module.exports = connect(mapStateToProps)(Reset)
