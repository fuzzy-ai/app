React = require 'react'
{ Link } = require 'react-router'

module.exports = React.createClass
  displayName: "Terms of Service"

  # coffeelint: disable=max_line_length
  render: ->
    <div className="row">
    <div className="main-80 small-text">
      <h2 className="mbt fwb">Terms of Service</h2>
      <p>This document, together with the Privacy Policy and any other policies or guidelines posted at <Link to="http://fuzzy.ai">fuzzy.ai</Link> (the “Agreement”) is a contract between you (“you”) and 9165584 Canada Corporation D/B/A Fuzzy.ai (“COMPANY”) concerning the conditions associated with your use of the website http://fuzzy.ai and/or of the services offered within such website (the “Service”). Please read all of the following terms and conditions carefully before using the Service. By using or accessing the Service, you agree to all the terms and conditions stated in this Agreement. If, at any time, you do not or cannot agree to the terms of this Agreement, you must stop accessing or using the Service.</p>

    <h3 className="mbt fwb">Conditions relating to use of the Service</h3>

    <p>You need a supported Web browser to access the Service. You acknowledge and agree that COMPANY may cease to support a given Web browser and that your continuous use of the Service will require you to download a supported Web browser. You also acknowledge and agree that the performance of the Service is incumbent on the performance of your computer equipment and your Internet connection.</p>

    <p>You are solely responsible for managing your account and password and for keeping your password confidential. If you have forgotten your password, click on the &quot;Forgot Password?&quot; link and follow the on-screen instructions. You are also solely responsible for restricting access to your account.</p>

    <p>You agree that you are responsible for all activities that occur on your account or through the use of your password by yourself or by other persons. If you believe that a third party obtained or guessed your password, use the password regeneration feature of the Service as soon as possible to obtain a new password.</p>

    <p>In all circumstances, you agree not to permit any third party to use or access the Service. </p>

    <p><strong>As a condition to your use of the Service, you agree not to:</strong></p>
      <ol>
        <li>Impersonate or misrepresent your affiliation with any person or entity;</li>
        <li>Access, tamper with, or use any non-public areas of the Service or COMPANY’s computer systems;</li>
        <li>Attempt to probe, scan, or test the vulnerability of the Service or any related system or network or breach any security or authentication measures used in connection with the Service and such systems and networks;
        </li>
        <li>Attempt to decipher, decompile, disassemble, or reverse engineer any of the software used to provide the Service;
        </li>
        <li>Harm or threaten to harm other users in any way or interfere with, or attempt to interfere with, the access of any user, host or network, including without limitation, by sending a virus, overloading, flooding, spamming, or mail-bombing the Service;
        </li>
        <li>Provide payment information belonging to a third party;</li>
        <li>Use the Service in an abusive way contrary to its intended use, to its documentation or to COMPANY’s reasonable instructions;
        </li>
        <li>Systematically retrieve data or other content from the Service to create or compile, directly or indirectly, in single or multiple downloads, a collection, compilation, database, directory or the like, whether by manual methods, through the use of bots, crawlers, or spiders, or otherwise;
        </li>
        <li>Infringe third party intellectual property rights when using or accessing the Service.
        </li>
      </ol>
      <p>COMPANY grants you the personal, non-transferable, non-exclusive, revocable and limited right to access and use the Service for your own personal purposes as an individual consumer.</p>

      <p>You agree that COMPANY may at any time modify the pricing scheme and/or fees attached to the subscription plan and your continuing use or access of the Service shall be deemed to constitute acceptance of such new pricing scheme or fee,</p>

      <p>COMPANY will have the right to investigate and prosecute violations of any of the above, including without limitation possible infringement of any intellectual property rights and possible security breaches, to the fullest extent of the law. COMPANY may involve and cooperate with law enforcement authorities in prosecuting users who violate this Agreement. you acknowledge that, although COMPANY has no obligation to monitor your access to or use of the Service, it has the right to do so for the purpose of operating the Service, to ensure your compliance with this Agreement, or to comply with applicable law or the order or requirement of a court, administrative agency, or other governmental body. </p>

      <h3 className="mbt fwb">Fees and Payment</h3>
      <p>COMPANY shall charge all applicable fees on the credit card associated to your COMPANY account. You are responsible for providing COMPANY valid credit card information. You may modify this information at any time by accessing the &ldquo;Credit Card&rdquo; section of the Service. You must update your credit card information if your credit card information changes.</p>

      <p>COMPANY accepts no liability to complete any transaction which cannot be cleared by our payment processor, whether because there are no sufficient funds available on your credit card or otherwise. If such situation would occur, you will receive an error message from the Service and the transaction will be denied. Moreover, COMPANY may suspend your account and contact you so you can provide COMPANY with valid alternative credit card information. Such suspension shall not relieve you from your obligation to pay any fees owed to COMPANY. </p>

      <p>All monies stipulated in this Agreement and in the Service are expressed in US dollars and include all applicable taxes.  </p>

      <h3 className="mbt fwb">Termination</h3>
      <p>You may cancel your individual account at any time by cancelling your account subscription. COMPANY also reserves the right to suspend or end the Service at any time at its discretion and without notice. For example, COMPANY may suspend or terminate your use of the Service if you are not complying with the present Agreement, or use the Service in a manner that would cause COMPANY legal liability, disrupt the Service or disrupt others' use of the Service. COMPANY reserves the right to terminate and delete your account if you haven't accessed the Service for 12 consecutive months.</p>

      <p>COMPANY reserves the right to temporarily or permanently discontinue the Service at any time. COMPANY will deploy commercially reasonable efforts to notify you of such discontinuation. If you breach this Agreement, COMPANY will have the right to terminate this Agreement immediately, without notice, and to deactivate your account.</p>

      <h3 className="mbt fwb">Proprietary Rights</h3>
      <p>All right, title, and interest in and to the Service are and will remain the exclusive property of COMPANY and its licensors. The Service is protected by copyright, trademark, and other domestic and foreign laws concerning intellectual property. Except as expressly permitted in this Agreement, you may not reproduce, modify, or create derivative works based upon, distribute, sell, transfer, publicly display, publicly perform, transmit, or otherwise use or commercially exploit the Service.
      </p>

      <h3 className="mbt fwb">Disclaimer of Warranty and Limitation of Liability</h3>
      <p>TO THE FULL EXTENT ALLOWED BY APPLICABLE LAW, COMPANY MAKES NO WARRANTY OR REPRESENTATION REGARDING THE SERVICE, INCLUDING THAT THE SERVICE WILL MEET YOUR REQUIREMENTS OR WILL WORK IN COMBINATION WITH ANY HARDWARE OR SOFTWARE PROVIDED BY THIRD PARTIES, THAT THE SERVICE WILL BE UNINTERRUPTED, WITHOUT PROBLEMS OR ERROR FREE, OR THAT ALL ERRORS IN THE SERVICE WILL BE CORRECTED. COMPANY PROVIDES THE SERVICE &ldquo;AS IS&rdquo; AND &ldquo;AS AVAILABLE&rdquo;.</p>

      <p>TO THE FULL EXTENT ALLOWED BY APPLICABLE LAW, COMPANY&rsquo; WARRANTIES AND REMEDIES (IF ANY) EXPRESSLY SET FORTH HEREIN ARE EXCLUSIVE AND ARE IN LIEU OF ALL OTHER WARRANTIES, EXPRESS OR IMPLIED, EITHER IN FACT OR BY OPERATION OF LAW, STATUTE, CUSTOM, ORAL OR WRITTEN STATEMENTS OR OTHERWISE, INCLUDING, BUT NOT LIMITED, TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, AVAILABILITY, PERFORMANCE, COMPATIBILITY, FITNESS FOR A PARTICULAR PURPOSE, SATISFACTORY QUALITY, CORRESPONDENCE WITH DESCRIPTION AND NONINFRINGEMENT, ALL OF WHICH ARE EXPRESSLY DISCLAIMED. </p>

      <p>TO THE FULL EXTENT ALLOWED BY APPLICABLE LAW, IN NO EVENT SHALL COMPANY AND ITS SUPPLIERS OR LICENSORS HAVE ANY LIABILITY, WHETHER BASED IN CONTRACT, DELICT OR TORT (INCLUDING NEGLIGENCE) OR STRICT LIABILITY, FOR INCIDENTAL, INDIRECT, CONSEQUENTIAL, SPECIAL, OR PUNITIVE DAMAGES OF ANY KIND, OR FOR LOSS OF REVENUE OR PROFITS, LOSS OF BUSINESS OR GOODWILL, LOSS OR CORRUPTION OF, OR UNAUTHORIZED ACCESS TO, OR DISCLOSURE OF INFORMATION OR DATA OR OTHER FINANCIAL LOSS ARISING OUT OF OR IN CONNECTION WITH THE USE, PERFORMANCE, FAILURE, OR INTERRUPTION OF THE SERVICE, WHETHER FORESEEABLE OR NOT, AND EVEN IF COMPANY HAD BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. IN THE EVENT THAT COMPANY IS FOUND LIABLE TO PAY YOU ANY DAMAGES, COMPANY&rsquo;S TOTAL CUMULATIVE LIABILITY TO YOU UNDER THIS AGREEMENT SHALL NOT EXCEEED $100. THE ABOVE LIMITATIONS OF LIABILITY WILL NOT BE AFFECTED IF ANY REMEDY PROVIDED HEREIN SHALL FAIL ITS ESSENTIAL PURPOSE. </p>
      <h3 className="mbt fwb">Indemnification</h3>
      <p>You agree to defend, indemnify, and hold harmless COMPANY, its officers, directors, affiliates, employees and agents, from and against any claims, liabilities, damages, losses, and expenses, including, without limitation, reasonable legal and accounting fees, arising out of or in any way connected with your access to or use of the Service.</p>
      <h3 className="mbt fwb">Amendments</h3>
      <p>COMPANY may amend this Agreement or any section of this Agreement, in whole or in part, at any time. Each such amendment (i) will be communicated to you at least 30 days before its coming into force; (ii) set out the new section only, or an amended section along with the section as it read formerly; (iii) set out the date the amendment comes into force; and (iv) if the amendment entails an increase in your obligations or a reduction in COMPANY&rsquo;s obligations, give you the right to refuse any amendment and terminate this Agreement without cost, penalty or cancellation indemnity by sending COMPANY a notice to that effect no later than 30 days after the amendment comes into force. In order to send such a notice, click on the &ldquo;I Do Not Agree&rdquo; when the amendment is communicated to you. Refusing an amendment entails the termination of this Agreement, the automatic deactivation of your account and denial of access to the Service.
      </p>
      <h3 className="mbt fwb">General Provisions</h3>
      <p>This Agreement shall be governed by and construed by the laws of the Province of Quebec, Canada and the laws of Canada applicable to contracts between Quebec residents and to be performed in Quebec. Parties hereby irrevocably submit and attorn to the jurisdiction of the Courts of the district of Montreal, Province of Qu&eacute;bec.</p>

      <p>This Agreement is the entire and exclusive agreement between COMPANY and you regarding the Service, and this Agreement supersedes and replaces any prior agreements between COMPANY and you regarding the Service.</p>

      <p>You shall not assign or otherwise transfer this Agreement or any of its rights or obligations hereunder to any third party without the prior written consent of COMPANY which consent is within COMPANY&rsquo;s sole discretion. No assignment or delegation by you shall relieve or release you from any of its obligations under this Agreement. Subject to the foregoing, this Agreement shall be binding upon, inure to the benefit of, and be enforceable by each of the Parties and their respective successors and assigns. COMPANY shall be allowed to assign this Agreement to any third party without requiring your consent.</p>

      <p>Nothing in this Agreement shall constitute a partnership or joint venture between you and COMPANY. </p>

      <p>If a particular provision of this Agreement is held to be invalid by a court of competent jurisdiction, the provision shall be deemed severed from this Agreement and shall not affect the validity of this Agreement as a whole.  </p>

      <p>The Parties have expressly requested that this Agreement be drawn up in English and that all modifications thereof can be made in this language. <em>Les Parties ont express&eacute;ment demand&eacute; que ce contrat soit r&eacute;dig&eacute; en anglais et que toute modification &agrave; celui-ci puisse se faire &eacute;galement dans cette langue.</em></p>

      <h3 className="mbt fwb">Contact</h3>
      <p>If you have any concern, question or complaint regarding this Agreement, please contact COMPANY at: 9165584 Canada Corporation D/B/A Fuzzy.ai
      <br />
      5333 AVE CASGRAIN SUITE 1227 MONTREAL,
      <br />
      QC H2T 1X3 legal@fuzzy.ai
      </p>
      <p><small>The current Agreement last update was June 23, 2015.</small> </p>



    </div>
    </div>
