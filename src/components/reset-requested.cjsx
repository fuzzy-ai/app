React = require 'react'
{ Link } = require 'react-router'

module.exports = React.createClass
  displayName: "Reset Requested"

  # coffeelint: disable=max_line_length
  render: ->
    <div className="row">
      <div className="page">
        <div className="password_request message messsage_plain">
          <h1 className="h2">Password reset requested</h1>
          <p>
            A password reset link will be sent to that email address if
            we find a corresponding account.
          </p>
          <p>
            Check your inbox for further instructions.
          </p>
        </div>
      </div>
    </div>
