_ = require 'lodash'
React = require 'react'
Link = require 'react-router/lib/Link'
Plan = require './plan'
{ connect } = require 'react-redux'
{ fetchPlans } = require '../actions/plans'
ErrorBar = require './error-bar'
PageTitle = require './page-title-header-section'


class Pricing extends React.Component
  displayName: "Pricing"

  @fetchData: (dispatch, params) ->
    dispatch(fetchPlans())

  componentDidMount: ->
    { dispatch } = @props
    dispatch(fetchPlans())

  # coffeelint: disable=max_line_length
  render: ->
    {plans, authenticated, user} = @props
    current = _.find plans, {stripeId: user?.plan}
    <div className="row site-wrap">
      {if plansError?
        <ErrorBar err={plansError} />
      }
      {if userError?
        <ErrorBar err={userError} />
      }

      <PageTitle
        title ="Pricing."
        subtitle ='Start adding intelligence to your software immediately with our flexible plans.'
        desc1 ='Start testing Fuzzy.ai within your code at no cost. Our free
        plan provides more than enough API calls to do your development and
        testing.'
        desc2 = null
        desc3 = null
      />

      <div className="page">
        <div className="pricing-zone flex-grid-wrap">
          {plans?.map (plan, i) ->
            <Plan key={i} plan={plan} current={current} authenticated={authenticated}/>
          }
          <div className="pricing-package plan-offered flex-grid-col">
            <div className="plan-header">
              <h3 className="plan-header__name spaced-normal fwh upper ">Enterprise plan</h3>
            </div>
            <ul className="plan compressed-list">
              <li className="compressed-list__item"><strong>Unlimited </strong>Monthly API Calls</li>
              <li className="compressed-list__item"><strong>Chat & Email </strong>Support</li>
              <li className="compressed-list__item"><strong>Telephone </strong>Support</li>
              <li className="compressed-list__item"><strong>Dedicated </strong>Account Executive</li>
            </ul>
            <div className="price-btn-wrapper">
              <a className="btn btn__regular" href="mailto:support@fuzzy.ai?subject=I%20would%20like%20some%20information%20about%20Enterprise%20plans">Contact Us</a>
            </div>
          </div>
        </div>
      </div>
    </div>


mapStateToProps = (state) ->
  plans: state.plans.items
  plansError: state.plans.error
  authenticated: state.user.authenticated
  user: state.user.account
  userError: state.user.error

module.exports = connect(mapStateToProps)(Pricing)
