React = require 'react'
{ Link } = require 'react-router'


module.exports = React.createClass
  displayName: "User Nav"

  render: ->
    <div className="login-menu-not">
      <ul className="login-nav">
      <li className="login-nav__item"><Link to="/signup" className="signup user-action">Get Started</Link></li>
      <li className="login-nav__item"><Link to="/login" className="login user-action">Log in</Link></li>
      </ul>
    </div>
