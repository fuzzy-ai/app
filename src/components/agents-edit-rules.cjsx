React = require 'react'
{ connect } = require 'react-redux'

{ storeAgent, editRule, deleteRule } = require '../actions/agent'
NewRule = require './agents-edit-new-rule'
IfThenRule = require './agents-edit-rule-if-then'
DecreasesRule = require './agents-edit-rule-decreases'
IncreasesRule = require './agents-edit-rule-increases'

AgentsRules = React.createClass
  displayName: 'EditRules'

  getInitialState: ->
    editMode: false

  toggleEditMode: ->
    @setState editMode: not @state.editMode

  handleStoreAgent: (agent) ->
    { dispatch } = @props
    dispatch(storeAgent(agent))

  handleEditRule: (agent, rule, id) ->
    { dispatch } = @props
    dispatch(editRule(agent, rule, id))

  handleDeleteRule: (agent, id) ->
    { dispatch } = @props
    dispatch(deleteRule(agent, id))


  # coffeelint: disable=max_line_length
  render: ->
    { agent, inProgress } = @props
    { editMode } = @state
    { handleStoreAgent, handleEditRule, handleDeleteRule, toggleEditMode } = @
    <div className="agent__element">
      { agent?.parsed_rules?.map (rule, i) ->
        switch rule.type
          when 'if-then'
            <IfThenRule key={i} id={i} rule={rule} agent={agent} globalEdit={editMode} toggleGlobalEdit={toggleEditMode} submitHandler={handleEditRule} deleteHandler={handleDeleteRule} agentHandler={handleStoreAgent} inProgress={inProgress} />
          when 'increases'
            <IncreasesRule key={i} id={i} rule={rule} agent={agent} globalEdit={editMode} toggleGlobalEdit={toggleEditMode} submitHandler={handleEditRule} deleteHandler={handleDeleteRule} inProgress={inProgress} />
          when 'decreases'
            <DecreasesRule key={i} id={i} rule={rule} agent={agent} globalEdit={editMode} toggleGlobalEdit={toggleEditMode} submitHandler={handleEditRule} deleteHandler={handleDeleteRule} inProgress={inProgress} />
      }
      <NewRule />
    </div>

mapStateToProps = (state) ->
  agent: state.agent.current
  inProgress: state.agent.inProgress
  error: state.agent.error

module.exports = connect(mapStateToProps)(AgentsRules)
