_ = require 'lodash'
React = require 'react'
Link = require 'react-router/lib/Link'
{ connect } = require 'react-redux'

{ fetchPlans } = require '../actions/plans'

ProgressBar = require './progress-bar'
ErrorBar = require './error-bar'

Upgraded = React.createClass
  displayName: "Upgraded"

  componentDidMount: ->
    { dispatch } = @props
    dispatch(fetchPlans())

  # coffeelint: disable=max_line_length
  render: ->
    { error, inProgress } = @props
    plan = _.find(@props.plans, {stripeId: @props.user?.plan})
    <div className="row">
      {if error?
        <ErrorBar err={error} />
      }
      <ProgressBar inProgress={inProgress} />
      <div className="main messsage_plain">
        <h2>Thank you for upgrading!</h2>

        <p>You are now on the <em>{plan?.name}</em>. You will receive an email shortly with your receipt.</p>

        <p><Link to="/">Go back to your dashboard</Link></p>
      </div>
    </div>

mapStateToProps = (state) ->
  user: state.user.account
  plans: state.plans.items
  inProgress: state.plans.inProgress
  error: state.plans.error

module.exports = connect(mapStateToProps)(Upgraded)
