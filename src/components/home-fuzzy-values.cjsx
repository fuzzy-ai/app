React = require 'react'
{ Link } = require 'react-router'

module.exports = React.createClass
  displayName: "home fuzzy values"

  # coffeelint: disable=max_line_length
  render: ->
    <div className="value__content">
      <h2 className=" value__content-h2">{@props.name}</h2>
      <p>{@props.description}</p>
      <div className="value__btn-wrapper">
        <Link className="btn  btn__regular  btn__anim" to={@props.btnLink} role="button"> {@props.btnName} </Link>
      </div>
    </div>
