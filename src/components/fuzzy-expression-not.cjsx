_ = require 'lodash'
React = require 'react'

FuzzyExpression = require './fuzzy-expression'

NotExpression = React.createClass
  displayName: 'NotExpression'

  # coffeelint: disable=max_line_length
  render: ->
    { json, agent, editMode } = @props

    exprProps =
      json: json.expression
      handleChange: @props.handleChange
      agent: agent
      editMode: editMode

    <span className="rule-form__expression">
      <span className="rule-form__var-expression-type not-expression"> not </span>
      <FuzzyExpression {...exprProps} />
    </span>

module.exports = NotExpression
