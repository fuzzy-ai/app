_ = require 'lodash'
React = require 'react'
{ Link } = require 'react-router'
{ connect } = require 'react-redux'


UpgradePrompt = require './upgrade-prompt'


{ fetchAgents } = require '../actions/agent'
{ fetchLogs } = require '../actions/logs'
{ fetchPlan } = require '../actions/plans'
{ fetchStats } = require '../actions/stats'

OFFSET = 0
LIMIT = 5

class DashboardCurrentPlan extends React.Component
  displayName: 'Dashboard Current Plan'

  componentDidMount: ->
    { dispatch, user } = @props
    dispatch(fetchAgents())
    if user?
      dispatch(fetchPlan(user.plan or "free"))
    dispatch(fetchLogs(OFFSET, LIMIT + 1))
    now = new Date()
    year = now.getUTCFullYear()
    month = now.getUTCMonth() + 1
    dispatch(fetchStats(year, month))

    agent_list = {}

  previousMonth: =>

    {dispatch, year, month} = @props

    if month == 1
      previousYear = year - 1
      previousMonth = 12
    else
      previousYear = year
      previousMonth = month - 1

    dispatch(fetchStats(previousYear, previousMonth))

    false

  nextMonth: =>

    {dispatch, year, month} = @props

    if month == 12
      nextYear = year + 1
      nextMonth = 1
    else
      nextYear = year
      nextMonth = month + 1

    dispatch(fetchStats(nextYear, nextMonth))

    false

  # coffeelint: disable=max_line_length
  render: ->
    { logs, plan, byDay, total, year, month, user} = @props
    { userInProgress, logsInProgress,  plansInProgress} = @props
    inProgress =  userInProgress || logsInProgress  || plansInProgress

    now = new Date()
    thisYear = now.getUTCFullYear()
    monthNames = [
      "January"
      "February"
      "March"
      "April"
      "May"
      "June"
      "July"
      "August"
      "September"
      "October"
      "November"
      "December"
    ]
    thisMonth = now.getUTCMonth() + 1
    signup = new Date(user?.createdAt)
    signupYear = signup.getUTCFullYear()
    signupMonth = signup.getUTCMonth() + 1

    monthName = monthNames[month - 1]

    <div className="dash-modules">
      <span className="sup-heading line upper fwb mbst">Current plan: {plan?.name}</span>
      { if (plan?.price == 0)
        <Link to="/pricing" className="dash-modules__data upper">Upgrade</Link>
      }
    </div>


mapStateToProps = (state) ->
  agentInProgress: state.agent.inProgress
  userInProgress: state.user.inProgress
  logsInProgress: state.logs.inProgress
  statsInProgress: state.stats.inProgress
  plansInProgress: state.plans.inProgress
  agents: state.agent.items
  user: state.user.account
  authenticated: state.user.authenticated
  logs: state.logs.logs
  plan: state.plans.current
  year: state.stats.year
  month: state.stats.month
  total: state.stats.total
  byDay: state.stats.byDay


module.exports = connect(mapStateToProps)(DashboardCurrentPlan)
