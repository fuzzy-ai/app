React = require 'react'
{ Link } = require 'react-router'
{ connect } = require 'react-redux'

{ resetPassword } = require '../actions/user'

ProgressBar = require './progress-bar'
ErrorBar = require './error-bar'

ResetPassword = React.createClass
  displayName: 'ResetPassword'

  getInitialState: ->
    errors: {}

  validate: (values) ->
    errors = {}
    if (!values.password)
      errors.password = "Password required."

    if (values.password.length < 8)
      errors.password = "Password must be 8 characters or more."

    if (values.confirmation != values.password)
      errors.confirmation = "Password and confirmation must match."

    if Object.keys(errors).length
      @setState errors: errors
      false
    else
      true

  handleSubmit: (e) ->
    e.preventDefault()

    { dispatch } = @props
    data =
      password: @refs.password.value
      confirmation: @refs.confirmation.value

    if @validate data
      @setState errors: {}
      dispatch resetPassword(data)

  # coffeelint: disable=max_line_length
  render: ->
    { errors } = @state
    { inProgress, error } = @props.user
    <div className="row">
      <div className="page">
        {if error?
          <ErrorBar err={error} />
        }
        <ProgressBar inProgress={inProgress} />
        <div className="form-wrapper">
          <form onSubmit={@handleSubmit} className="user-forms">
            <div className="form-group input-wrap">
              <label className="upper spaced-medium" htmlFor="password">New Password</label>
              <input className="form-control" id="password" ref="password" placeholder="New Password" required="required" type="password" />
              { if errors?.password?
                <label className="error" htmlFor="password">{ errors.password }</label>
              }
            </div>
            <div className="form-group input-wrap">
              <label className="upper spaced-medium" htmlFor="confirmation">Retype New Password</label>
              <input className="form-control" ref="confirmation" placeholder="Retype New Password" required="required" type="password" />
              { if errors?.confirmation?
                <label className="error" htmlFor="confirmation">{ errors.confirmation }</label>
              }
            </div>
            <button className="btn">Update</button>
          </form>
        </div>
      </div>
    </div>

mapStateToProps = (state) ->
  return {
    user: state.user
  }

module.exports = connect(mapStateToProps)(ResetPassword)
