_ = require 'lodash'
React = require 'react'

{ connect } = require 'react-redux'
{ newAgent } = require '../actions/agent'
{ fetchTemplates } = require '../actions/templates'

SetOutput = require './agents-edit-set-output'

ProgressBar = require './progress-bar'
ErrorBar = require './error-bar'

NewAgentEmpty = React.createClass
  displayName: 'NewEmptyAgent'

  getInitialState: ->
    agent:
      inputs: {}
      outputs: {}
      rules: []

  handleSubmit: (agent) ->
    { dispatch } = @props
    agent.name = _.keys(agent.outputs)[0]
    @setState agent: agent

    dispatch newAgent(agent)

  # coffeelint: disable=max_line_length
  render: ->
    {agent, mode} = @state
    { error, inProgress } = @props
    <section className="new-agent-wrap">
      {if error?
        <ErrorBar err={error} />
      }
      <ProgressBar inProgress={inProgress} />
      <SetOutput agent={agent} submitHandler={@handleSubmit} />
    </section>

mapStateToProps = (state) ->
  templates: state.agent.templates
  inProgress: state.agent.inProgress
  error: state.agent.error

module.exports = connect(mapStateToProps)(NewAgentEmpty)
