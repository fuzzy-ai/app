React = require 'react'
{ Link } = require 'react-router'

module.exports = React.createClass
  displayName: "Agents"

  render: ->
    <div className="row app-row-bg">
      <div className="page">
      {@props.children}
      </div>
    </div>
