React = require 'react'
{ Bar } = require 'react-chartjs-2' unless not window?
_ = require 'lodash'
moment = require 'moment'

{ Link } = require 'react-router'
{ connect } = require 'react-redux'

class APIUsageChart extends React.Component

  displayName: 'API Usage Chart'

  render: ->
    { byDay, width, height } = @props
    if not byDay or not window?
      return null

    width or= "100%"

    numDays = moment(_.keys(byDay)[0]).daysInMonth()
    data =
      labels: _.map(_.keys(byDay), (date) -> date.slice(8)).slice(0, numDays)
      datasets: [
        {
          label: "API calls",
          yPadding: 20,
          xPadding: 20,
          borderWidth: 1,
          backgroundColor: "#705782",
          borderColor: "#412852",
          hoverBackgroundColor: 'rgba(73, 48, 91, 0.85)',
          hoverBorderColor: '#231a26',
          data: _.values(byDay).slice(0, numDays)
        }
      ]

    maxHits = Math.max(_.max(data.datasets[0].data), 120)

    n = 10
    while (n * 20) <= maxHits
      n *= 10

    chartOptions =
      responsive: true
      maintainAspectRatio: true
      animationEasing: "easeOutQuart"

      tooltips: {
        enabled: true
        backgroundColor: "rgba(36, 28, 41, 0.9)"
        titleFontSize: 11
        titleFontColor: "#FFFDF2"
        titleFontStyle: "bold"
        titleSpacing: 2
        titleMarginBottom: 6
        bodyFontColor: "#DCE0E4"
        bodyFontSize: 13
        xPadding: 10
        yPadding: 10
      }


      legend: {
        display: true
        position: 'top'
        fullWidth: true
        fill: true

        labels: {

          fill: true
          fontColor: '#423647'
          boxWidth: 40
          fontSize: 12
        }
      }


      scales:

        xAxes: [{
          categoryPercentage: 0.98
          gridLines:
            show: true
            color: "#DCE0E4"


          ticks:
            fontSize: 12
            autoSkip:true
            maxTicksLimit: 20
            maxRotation: 0
            fontColor: "#423647"
        }]
        yAxes: [{
          type: 'linear'

          gridLines:
            show: true
            color: "transparent"


          ticks:
            min: 0
            max: maxHits
            stepSize: n
            fontSize: 10
            autoSkip:true
            fontColor: "#694B7C"
        }]



    <Bar data={data} options={chartOptions} id="api-chart" />

mapStateToProps = (state) ->
  year: state.stats.year
  month: state.stats.month
  total: state.stats.total
  byDay: state.stats.byDay

module.exports = connect(mapStateToProps)(APIUsageChart)
