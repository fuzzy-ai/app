_ = require('lodash')

React = require 'react'
Link = require 'react-router/lib/Link'
{ push } = require 'react-router-redux'

{ connect } = require 'react-redux'
{ newAgent } = require '../actions/agent'
{ fetchTemplates } = require '../actions/templates'
ErrorBar = require './error-bar'

class TemplateList extends React.Component
  displayName: 'Template list'

  # coffeelint: disable=max_line_length
  render: ->
    { templates, onTemplateClick } = @props
    <div className="new-agent flex-grid-wrap">
    {templates?.map (template, i) ->
      <TemplateListItem template={template} key={i} onTemplateClick={onTemplateClick}/>
    }
    </div>

class TemplateListItem extends React.Component
  displayName: 'Template list item'

  render: ->
    { template, onTemplateClick } = @props
    <div className="new-agent__card flex-grid-col">
      <a href="#" className="new-agent__card-anchor-wrap"  data-slug={template.slug} onClick={(e) -> onTemplateClick(e, template.slug)} >
        <h2 className="upper new-agent__name fwh">{template.name}</h2>
        <p>{template.description}</p>
      </a>
    </div>

connect({}, TemplateListItem)

NewAgent = React.createClass
  displayName: 'New Agent'

  componentDidMount: ->
    { dispatch } = @props
    dispatch(fetchTemplates())

  handleClick: (e, slug) ->
    { dispatch, templates } = @props
    e.preventDefault()

    template = _.find templates, {slug: slug}

    agent = template.source
    if slug == 'empty'
      dispatch(push('/agents/new/empty'))
    else
      agent.name = template.name
      dispatch(newAgent(agent))

  render: ->
    { templates, error } = @props
    if templates
      if not _.find templates, {slug: 'empty'}
        templates.push
          slug: 'empty'
          name: 'Empty'
          description: 'An empty agent - build from scratch!'

    <section className="new-agent-wrap">
      {if error?
        <ErrorBar err={error} />
      }
      <main className="page-intro-zone">
        <header className="main-intro-text">
          <h1 className="page-title-wrap__heading inline big-title">New Agent</h1>
          <p className="big-para">Choose a template or start with an empty agent.</p>
        </header>
      </main>
      <div className="new-agent-card-wrap">
        <TemplateList templates={templates} onTemplateClick={@handleClick}/>
      </div>
    </section>



mapStateToProps = (state) ->
  templates: state.templates.templates
  error: state.templates.error

module.exports = connect(mapStateToProps)(NewAgent)
