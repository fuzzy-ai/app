_ = require 'lodash'
moment = require 'moment'
React = require 'react'
{ Link } = require 'react-router'

InvoiceItem = React.createClass
  displayName: 'InvoiceItem'

  # coffeelint: disable=max_line_length
  render: ->
    { invoice } = @props
    <tr className="invoice">
      <td className="date">{moment.unix(invoice.date).format('YYYY-MM-DD')}</td>
      <td className="amount">${invoice.amount_due / 100}</td>
      <td>
        {if invoice.paid
          <span className="paid">PAID </span>
        }
        <a href="/invoices/#{invoice.id}">view</a>
      </td>
    </tr>

module.exports = React.createClass
  displayName: 'BillingInfoSettings'

  # coffeelint: disable=max_line_length
  render: ->
    plan = _.find(@props.plans, {stripeId: @props.user?.plan})
    <div className="module billing-info">
      <h2>Billing Information</h2>
      <p>You are currently using the <strong>{plan?.name}</strong></p>
      {if @props.invoices?.length
        <table>
          <thead>
            <tr>
              <th>Date</th>
              <th>Amount</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {@props.invoices.map (invoice, i) ->
              <InvoiceItem key={i} invoice={invoice} />
            }
          </tbody>
        </table>
      }
      <Link to="/pricing" className="btn">Upgrade plan</Link>
    </div>
