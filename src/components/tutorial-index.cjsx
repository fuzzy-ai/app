React = require 'react'
{ Link } = require 'react-router'

module.exports = React.createClass
  displayName: "Tutorial - Understanding The Problem"

  # coffeelint: disable=max_line_length
  render: ->
    <div className="page">
      <div className="tutorial">
        <div className="tutorial__intro-text">
         <span className="sup-heading line upper fwb mbt">Tutorial</span>
          <h1 className="fwb">Understanding the Problem</h1>
          <p>Fuzzy.ai lets you use your understanding of your problem to add intelligence to your application.</p>
          <p>So, consider this problem: you have started a lemonade stand on your street and you want to make sure you&apos;re charging the right price for your lemonade. The price could go up or down because of many different reasons, but let&apos;s start with the assumption that as the temperature outside increases, the lemonade&apos;s price should go up.</p>
          <p>In the next few pages, we will talk about using Fuzzy.ai to determine the best price for a cup of lemonade. Next step: create an agent to help with the calculation.</p>

        </div>
      </div>
      <div className="pagination tuts-pagination">
        <Link to="/tutorial/create-an-agent" className="btn btn-blue">Go to step two: <strong>creating an agent</strong></Link>
        <br />
        <Link to="/agents/new" className="secondary-tut-link">Skip tutorial</Link>
      </div>
    </div>
