React = require 'react'
{ Link } = require 'react-router'
{ connect } = require 'react-redux'
{ fetchAgent } = require '../actions/agent'
{ updateTutorial } = require '../actions/tutorial'

Clipboard = require './clipboard'
DashboardApiKey = require './dashboard-api-key'

TutorialIntegrate = React.createClass
  displayName: "Tutorial - Integrate into Your Code"

  componentDidMount: ->
    { dispatch } = @props
    if not @props.agent?.id and @props.tutorial?.agentID
      dispatch fetchAgent(@props.tutorial.agentID)
    else if not @props.tutorial?.agentID
      dispatch push('/tutorial/create-an-agent')

    dispatch updateTutorial({step: 'integrate-into-your-code'})

  # coffeelint: disable=max_line_length
  render: ->
    {agent} = @props

    <div className="page">
      <div className="tutorial">
        <h1>Integrate into Your Code</h1>
        <div className="tutorial__row">
          <div className="tutorial__text ">
            <p>Fuzzy.ai is available with a <Link to="/docs/rest">RESTful API</Link> that makes it easy to send data to your agent and get the results. </p>
            <p>There are also SDKs for a number of different programming languages. Let&apos;s imagine that your lemonade stand has NodeJS, so you can use the <a href="https://www.npmjs.com/package/fuzzy.ai">fuzzy.ai</a> package on npm. It can be installed using package.json or just  <code>npm install fuzzy.ai</code> on the command line.</p>
            <p> Once the fuzzy.ai package is installed, you can use <code>require</code> to get it into your code. You will need your API key from the Web UI; it&apos;s in the upper right-hand corner of the dashboard. </p>
          </div>
          <figure className="tutorial__image">
            <DashboardApiKey />
          </figure>
        </div>
        <div className="tutorial__row">
          <div className="tutorial__text ">
            <h2>Agent id </h2>
            <p>You&apos;ll also need the Agent ID; it&apos;s on the agent edit page.</p>
          </div>
          <figure className="tutorial__image">
            <span className="agent__id">ID:
              <Clipboard copyText={ agent?.id } />
            </span>
            <h1>
              <span className="app-heading">{ agent?.name }</span>
            </h1>
          </figure>
        </div>
        <div className="tutorial__row">
          <div className="tutorial__text">
            <p>To call your agent and ask for the price of a cup of lemonade, you create a <code>FuzzyAIClient</code> object in your code, and then send the inputs as a JSON object. </p>
            <p>Now that you have the agent integrated to your code, you can start providing feedback on the performance of the agent to improve its output.</p>
          </div>
          <div className="tutorial__code">
            <pre className="pad-40 radius-4">
              {"""
                var FuzzyAIClient = require('fuzzy.ai');

                var API_KEY = 'your API key here';
                var AGENT_ID = 'your agent ID here';

                var client = new FuzzyAIClient(API_KEY);

                client.evaluate(AGENT_ID, {Temperature: 90}, function(err, outputs)) {
                  if (err) {
                    console.error(err);
                  } else {
                    console.log(outputs['price per cup']);
                  }
                });
              """}
            </pre>
          </div>
        </div>
        <div className="pagination tuts-pagination">
          <Link to="/tutorial/provide-feedback" className="btn btn-blue">Go to step six: <strong>provide feedback</strong></Link>
          <br />
          <Link to="/tutorial/add-a-first-rule" className="secondary-tut-link previous-page-link">Go to previous page</Link>
          <Link to="/agents/new" className="secondary-tut-link">Skip tutorial</Link>
        </div>
      </div>
    </div>

mapStateToProps = (state) ->
  agent: state.agent.current
  tutorial: state.tutorial

module.exports = connect(mapStateToProps)(TutorialIntegrate)
