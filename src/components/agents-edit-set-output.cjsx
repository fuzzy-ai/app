React = require 'react'

RangeInput = require './range-input'

AgentsEditSetOutput = React.createClass
  displayName: 'SetOutput'

  getInitialState: ->
    showRange: false
    range: [0, 100]
    error: ''

  handleSubmit: (e) ->
    e.preventDefault()
    { agent, submitHandler } = @props
    if not @refs.output.value
      @setState error: "invalid output name"
    else if @state.range.length > 3 or @state.range.length < 2
      @setState error: "invalid range"
    else
      agent.outputs[@refs.output.value] = @state.range
      submitHandler(agent)

  handleRange: (range) ->
    @setState range: range

  # coffeelint: disable=max_line_length
  render: ->
    { error, showRange } = @state
    <div className="agent-empty">
      <div className="rule-form__text-wrap">
        {if error
          <p className="error_message">{error}</p>
        }
        <form onSubmit={@handleSubmit}>
          <div className="output-wrapper ">
            <h2>What do you want to calculate?</h2>
            <input ref="output" id="output" name='output' placeholder="Name your agent"/>
            <RangeInput handleRange={@handleRange} fromValue={@state.range[0]} toValue={@state.range[1]}/>
          </div>
          <button type="submit" className="btn btn-default">Save</button>
        </form>
      </div>
    </div>

module.exports = AgentsEditSetOutput
