React = require 'react'
{ Link } = require 'react-router'

HomeHero = require './home-hero'
HomeGrid = require './home-grid'

FuzzyClient = require './home-client'
FuzzyAsSeenOn = require './home-as-seen-on'


module.exports = React.createClass

  displayName: "Landing"


  renderDefs: ->
    <img srcSet="/assets/i/new-devices-retina-ai-v1-@1x.png 1x, /assets/i/new-devices-retina-ai-v1-@2x.png 2x" width="1400"  alt="Devices fuzzy.ai Responsive app"/>


  componentDidMount: ->
    scrolledElement = document.querySelector('.grid-wrap')
    top = scrolledElement?.offsetTop

    listener = ->
      y = window.pageYOffset
      if y >= 600
        scrolledElement.classList.add 'activate_load'
      return

    window.addEventListener 'scroll', listener, false


  # coffeelint: disable=max_line_length
  render: ->
    <div id="site">
      <HomeHero />
      <HomeGrid />
        <div className="secondary-cta fuzzy-home-values-2">
          <div className="secondary-cta__text page">
            <p data-text="Fuzzy.ai makes it easy to add intelligent decision making into any web or mobile application. Find the best price to charge for a product, identify your best customers, detect fraud.">
            Fuzzy.ai makes it easy to add intelligent decision making into any web or mobile application. Find the best price to charge for a product, identify your best customers, detect fraud.
            </p>
          </div>
        </div>

      <FuzzyAsSeenOn />

      <div className="row  macbook-zone">
        <div className="thedevicegoeshere">
          <picture>
           {this.renderDefs()}
          </picture>
        </div>
      </div>
      <FuzzyClient />
    </div>
