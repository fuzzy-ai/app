React = require 'react'
{ Link } = require 'react-router'

CompanyLogo = require ('./company-logo')

module.exports = React.createClass

  displayName: "home client"

  # coffeelint: disable=max_line_length
  render: ->
    <div className="row svg-zone-wrapper">
      <div className="svg-zone page">
        <div className="svg-zone__heading-wrap">
          <h2>Our customers include</h2>
        </div>
        <div className="svg-zone__inner">
          <CompanyLogo
            class= "breather cie-logo"
            svgId= "#breather-logo"
          />
          <CompanyLogo
            class= "intel-logo cie-logo"
            svgId= "#intel-logo"
          />
          <CompanyLogo
            class= "upcouncel-logo cie-logo"
            svgId= "#upcouncel-logo"
          />
          <CompanyLogo
            class= "startupfest-logo cie-logo"
            svgId= "#startupfest-logo"
          />
        </div>
      </div>
    </div>
