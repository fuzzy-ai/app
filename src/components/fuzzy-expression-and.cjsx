_ = require 'lodash'
React = require 'react'

FuzzyExpression = require './fuzzy-expression'

AndExpression = React.createClass

  getInitialState: ->
    left: @props.json?.left
    right: @props.json?.right

  componentDidUpdate: (prevProps, prevState) ->
    { handleChange, agent } = @props

    if not _.isEqual prevState, @state
      newJSON =
        type: 'and'
        left: @state.left
        right: @state.right

      handleChange newJSON, agent

  handleLeftChange: (expression, agent) ->
    @setState left: expression

  handleRightChange: (expression, agent) ->
    @setState right: expression

  handleDelete: (e) ->
    e.preventDefault()

    { handleChange, agent } = @props
    window.confirm("Are you sure?")


    if @state.right.type == 'is'
      # remove the right clause
      newJSON = @state.left
      handleChange newJSON, agent
    else
      @setState right: @state.right.right
      newJSON =
        type: @state.right.type
        left: @state.left
        right: @state.right.right
      handleChange newJSON, agent

  # coffeelint: disable=max_line_length
  render: ->
    { agent, editMode } = @props
    { left, right } = @state

    leftProps =
      json: left
      handleChange: @handleLeftChange
      agent: agent
      editMode: editMode

    rightProps =
      json: right
      handleChange: @handleRightChange
      agent: agent
      editMode: editMode

    <span className="rule-form__expression">
      <FuzzyExpression {...leftProps} />
      <span className="rule-form__var-expression-type" onClick={@handleDelete}> and </span>
      <FuzzyExpression {...rightProps} />
    </span>

module.exports = AndExpression
