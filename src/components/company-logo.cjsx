React = require 'react'
{ Link } = require 'react-router'

module.exports = React.createClass

  displayName: "company logo"

  render: ->
    <div className="cie-logo-wrap">
      <figure className="cie-logo-wrap__inner">
          <svg xmlns="http://www.w3.org/2000/svg" className={@props.class}  >
            <use xlinkHref="/assets/sprite.svg#{@props.svgId}"></use>
          </svg>
      </figure>
    </div>
