React = require 'react'
{ Link } = require 'react-router'


module.exports = React.createClass
  displayName: "DynamicPricing"

  # coffeelint: disable=max_line_length
  renderDefs: ->
    <linearGradient id="Gradient">
      <stop className="stop1" offset="35%" stopColor="#744d8d" />
      <stop className="stop2" offset="70%" stopColor="#53B4C4" />
      <stop className="stop3" offset="80%" stopColor="#694e7c" />
      <stop className="stop4" offset="100%" stopColor="#563b69" />
    </linearGradient>

  getDefaultProps: ->
    lineStyle:
      stroke: 'url(#Gradient)',
      strokeWidth: '1'

  # coffeelint: disable=max_line_length
  render: ->

    <div className="hero-wrapper">
      <div className="hero flex">
        <div className="hero__text">
          <h1 className=" hero__text__h1 upper">Ai Powered <span className="accent ">Dynamic</span> Pricing</h1>
          <h2 className=" hero__text__sub-heading upper">EASILY BUILD DYNAMIC PRICING INTO ANY SOFTWARE</h2>
          <p className="hero__text__para ">Fuzzy.ai is an API that makes it easy for any developer to build a dynamic pricing algorithm that improves automatically using machine learning. No training data or data scientists required. Try it now for free!
            <br />
            <a className="btn btn__primary" href="/signup" role="button">Sign up for free</a>
            <span className="no-credit-required line upper">No credit card required</span>
          </p>
        </div>


      </div>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 704 667" className="hero-lines" style={@props.lineStyle} >
        <defs>
         {this.renderDefs()}
        </defs>
        <g className="cls-1">
          <path id="Rounded_Rectangle_1" d="M558.91 399.294l13.436-13.435a4 4 0 0 1 5.657 0l302.64 302.64a4 4 0 0 1 0 5.656L867.21 707.59a4 4 0 0 1-5.658 0l-302.64-302.64a4 4 0 0 1 0-5.656z" data-name="Rounded Rectangle 1" className="cls-2"/>
          <path id="Rounded_Rectangle_1_copy" d="M523.555 397.88l47.376-47.38a4 4 0 0 1 5.658 0l391.03 391.03a4 4 0 0 1 0 5.656l-47.376 47.377a4 4 0 0 1-5.657 0l-391.03-391.03a4 4 0 0 1 0-5.653z" data-name="Rounded Rectangle 1 copy" className="cls-2"/>
          <path id="Rounded_Rectangle_1_copy_2" d="M490.32 397.173l79.2-79.2a4 4 0 0 1 5.658 0l630.732 630.743a3.986 3.986 0 0 1 0 5.657l-79.19 79.2a4.008 4.008 0 0 1-5.66 0L490.32 402.83a4 4 0 0 1 0-5.657z" data-name="Rounded Rectangle 1 copy 2" className="cls-2"/>
          <path id="Rounded_Rectangle_1_copy_3" d="M454.26 397.88l116.67-116.673a4 4 0 0 1 5.658 0l911.462 911.463a4 4 0 0 1 0 5.65L1371.38 1315a4.008 4.008 0 0 1-5.66 0L454.26 403.537a4 4 0 0 1 0-5.657z" data-name="Rounded Rectangle 1 copy 3" className="cls-2"/>
          <path id="Rounded_Rectangle_1_copy_4" d="M418.2 397.173l152.024-152.028a4 4 0 0 1 5.657 0L1753.21 1422.48a3.978 3.978 0 0 1 0 5.65l-152.02 152.03a4.008 4.008 0 0 1-5.66 0L418.2 402.83a4 4 0 0 1 0-5.657z" data-name="Rounded Rectangle 1 copy 4" className="cls-2"/>
        </g>
        <g id="group-1" data-name="group 1" className="cls-1">
          <path id="Rounded_Rectangle_1-2" d="M842.46 333.533L855.9 320.1a4 4 0 0 1 5.657 0l302.633 302.64a3.986 3.986 0 0 1 0 5.657l-13.43 13.435a4 4 0 0 1-5.66 0L842.46 339.19a4 4 0 0 1 0-5.657z" data-name="Rounded Rectangle 1" className="cls-2"/>
          <path id="Rounded_Rectangle_1_copy-2" d="M807.105 332.12l47.376-47.377a4 4 0 0 1 5.658 0l391.032 391.03a4 4 0 0 1 0 5.657l-47.38 47.376a3.992 3.992 0 0 1-5.65 0l-391.035-391.03a4 4 0 0 1 0-5.657z" data-name="Rounded Rectangle 1 copy" className="cls-2"/>
          <path id="Rounded_Rectangle_1_copy_2-2" d="M773.87 331.412l79.2-79.2a4 4 0 0 1 5.658 0l630.732 630.743a3.986 3.986 0 0 1 0 5.657l-79.19 79.2a4 4 0 0 1-5.66 0L773.87 337.07a4 4 0 0 1 0-5.658z" data-name="Rounded Rectangle 1 copy 2" className="cls-2"/>
          <path id="Rounded_Rectangle_1_copy_3-2" d="M737.81 332.12l116.67-116.674a4 4 0 0 1 5.658 0L1771.6 1126.91a4 4 0 0 1 0 5.65l-116.67 116.68a4.008 4.008 0 0 1-5.66 0L737.81 337.776a4 4 0 0 1 0-5.657z" data-name="Rounded Rectangle 1 copy 3" className="cls-2"/>
          <path id="Rounded_Rectangle_1_copy_4-2" d="M701.746 331.412l152.028-152.028a4 4 0 0 1 5.657 0L2036.76 1356.72a3.978 3.978 0 0 1 0 5.65l-152.02 152.03a4.008 4.008 0 0 1-5.66 0L701.746 337.07a4 4 0 0 1 0-5.658z" data-name="Rounded Rectangle 1 copy 4" className="cls-2"/>
        </g>
        <g id="Group_7_copy" data-name="Group 7 copy" className="cls-1">
          <path id="Rounded_Rectangle_1-3" d="M372.234-136.693l13.435-13.435a4 4 0 0 1 5.656 0l302.642 302.642a4 4 0 0 1 0 5.657l-13.435 13.436a4 4 0 0 1-5.657 0L372.234-131.036a4 4 0 0 1 0-5.657z" data-name="Rounded Rectangle 1" className="cls-2"/>
          <path id="Rounded_Rectangle_1_copy-3" d="M285.26-189.726l47.376-47.374a4 4 0 0 1 5.657 0l391.03 391.03a4 4 0 0 1 0 5.657l-47.376 47.376a4 4 0 0 1-5.657 0L285.26-184.07a4 4 0 0 1 0-5.656z" data-name="Rounded Rectangle 1 copy" className="cls-2"/>
          <path id="Rounded_Rectangle_1_copy_2-3" d="M46.258-396.2l79.2-79.2a4 4 0 0 1 5.657 0L761.85 155.342a4 4 0 0 1 0 5.657l-79.2 79.2a4 4 0 0 1-5.657 0L46.258-390.545a4 4 0 0 1 0-5.656z" data-name="Rounded Rectangle 1 copy 2" className="cls-2"/>
          <path id="Rounded_Rectangle_1_copy_3-3" d="M-235.17-640.86l116.67-116.673a4 4 0 0 1 5.657 0l911.463 911.46a4 4 0 0 1 0 5.658L681.947 276.257a4 4 0 0 1-5.657 0L-235.17-635.2a4 4 0 0 1 0-5.66z" data-name="Rounded Rectangle 1 copy 3" className="cls-2"/>
          <path id="Rounded_Rectangle_1_copy_4-3" d="M-500.335-870.67l152.028-152.03a4 4 0 0 1 5.656 0L834.68 154.635a4 4 0 0 1 0 5.657L682.654 312.32a4 4 0 0 1-5.657 0L-500.335-865.013a4 4 0 0 1 0-5.657z" data-name="Rounded Rectangle 1 copy 4" className="cls-2"/>
        </g>
        <g id="Group_7_copy_2" data-name="Group 7 copy 2" className="cls-1">
          <path id="Rounded_Rectangle_1-4" d="M90.806-67.4l13.435-13.435a4 4 0 0 1 5.658 0L412.54 221.81a4 4 0 0 1 0 5.657L399.105 240.9a4 4 0 0 1-5.657 0L90.806-61.74a4 4 0 0 1 0-5.66z" data-name="Rounded Rectangle 1" className="cls-2"/>
          <path id="Rounded_Rectangle_1_copy-4" d="M3.832-120.43l47.376-47.376a4 4 0 0 1 5.657 0L447.9 223.224a4 4 0 0 1 0 5.657l-47.376 47.377a4 4 0 0 1-5.657 0L3.832-114.773a4 4 0 0 1 0-5.657z" data-name="Rounded Rectangle 1 copy" className="cls-2"/>
          <path id="Rounded_Rectangle_1_copy_2-4" d="M-235.17-326.9l79.2-79.2a4 4 0 0 1 5.657 0l630.74 630.74a4 4 0 0 1 0 5.656l-79.2 79.2a4 4 0 0 1-5.658 0l-630.74-630.744a4 4 0 0 1 0-5.652z" data-name="Rounded Rectangle 1 copy 2" className="cls-2"/>
          <path id="Rounded_Rectangle_1_copy_3-4" d="M-516.6-571.564l116.673-116.672a4 4 0 0 1 5.657 0l911.46 911.46a4 4 0 0 1 0 5.657L400.52 345.555a4 4 0 0 1-5.658 0L-516.6-565.906a4 4 0 0 1 0-5.658z" data-name="Rounded Rectangle 1 copy 3" className="cls-2"/>
          <path id="Rounded_Rectangle_1_copy_4-4" d="M-781.764-801.373L-629.736-953.4a4 4 0 0 1 5.657 0L553.255 223.93a4 4 0 0 1 0 5.658L401.226 381.616a4 4 0 0 1-5.657 0L-781.765-795.717a4 4 0 0 1 0-5.656z" data-name="Rounded Rectangle 1 copy 4" className="cls-2"/>
        </g>
      </svg>
    </div>
