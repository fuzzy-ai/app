React = require 'react'
{ Link } = require 'react-router'
PageTitle = require './page-title-header-section'

module.exports = React.createClass
  displayName: "How it works"

  # coffeelint: disable=max_line_length
  render: ->
    <div className="row">
      <PageTitle
        title ="How it works."
        subtitle ='Fuzzy.ai is a service that lets you create agents that add intelligence to your applications.'
        desc1 = null
        desc2 = null
        desc3 = null
      />

      <div className="page">
        <div className=" how-works how-works-first">
          <div className="how-works__inner">

            <h2 className="fwb">You can get started right away.</h2>
            <span className="num big-digits fwh">01</span>
            <p>Anyone with experience building Web or mobile apps can easily
              understand Fuzzy.ai. You don’t need to be a machine learning expert
              or a data scientist to create Fuzzy.ai agents.</p>
            <p>You don’t need a big training set of data to get started, either.
            All you need is some understanding of your problem domain.</p>
          </div>
          <div className="how-works__imagery">
            <img src="assets/i/robo-lines.svg"  className="robot-1 robot"/>
          </div>
        </div>
        <div className=" how-works how-works-second">
          <div className="how-works__imagery">
            <img src="assets/i/robo-lines-if-then.svg" className="robot-2 robot" />
          </div>
          <div className="how-works__inner">
            <h2 className="fwb">Turning knowledge into code</h2>
            <span className="num big-digits fwh">02</span>

            <p>Fuzzy.ai lets you turn your domain knowledge into code.
            You create agents on the Fuzzy.ai Web site and tell them rules about
            how to make decisions.</p>
            <p>Our technique, called <a href="https://en.wikipedia.org/wiki/Fuzzy_logic">fuzzy logic</a>,
            lets your rules be like what people would understand.</p>
            <p><strong>Fuzzy.ai’s rules let you express ideas like:</strong>  </p>
              <ul className="compressed-list">
              <li className="compressed-list__item">Prices should be lower on weekends.</li>
              <li className="compressed-list__item">People who buy a shirt in a particular style may like another
                shirt with the same style.</li>
              <li className="compressed-list__item">Orders where the credit-card country are different from the
                delivery country are suspicious.</li>
              </ul>

          </div>

        </div>
        <div className=" how-works how-works-third">
          <div className="how-works__inner">
            <h2 className="fwb">Hooking up your applications</h2>
            <span className="num big-digits fwh">03</span>

            <p>Once you have created an agent, you can send data to it from your Web
             or mobile app code. It will send back answers that your app can use to
            change its interactions with the user.</p>
            <p>Fuzzy.ai provides a <a href="https://en.wikipedia.org/wiki/Representational_state_transfer">RESTful</a> API for your code. We have SDKs for many Web languages, including
            Python, PHP, Ruby, and Node.js.</p>
          </div>
          <div className="how-works__imagery">
            <img src="assets/i/robo-lines-hookup.svg" className="robot-3 robot" />
          </div>
        </div>
        <div className=" how-works how-works-fourth">

          <div className="how-works__imagery">
            <img src="assets/i/robo-lines-stronger.svg" className="robot-4 robot" />
          </div>
          <div className="how-works__inner">
            <h2 className="fwb">Getting better over time</h2>
            <span className="num big-digits fwh">04</span>

            <p>Once you’ve hooked up your application to Fuzzy.ai, you’ll get
            intelligent results immediately. You can improve those results over time
            by providing feedback.</p>

            <p>In your code, you can determine how well the Fuzzy.ai agent’s
            decision has done. Did the user buy the product at the recommended
            price? Was the order returned? Did the user click through on the
            recommended link?</p>

            <p>As you provide feedback, Fuzzy.ai will automatically tune your agent
            to give better results the next time you ask for an answer.</p>
          </div>

        </div>

        <div className=" how-works how-works-get-started">

          <div className="how-works__inner">
          <span className="sup-heading upper fwb line mbs">Free to try</span>
            <h2 className="fwb">No credit card required</h2>
            <span className="num big-digits fwh">05</span>

            <p>Fuzzy.ai is free to try. You get 5000 free evaluations per month, so
            you can start making decisions in your code immediately and see for
            yourself how it improves your software and delights your users.</p>

            <p>As you scale, Fuzzy.ai scales with you. We have a number of pro
            account levels that let you make hundreds of thousands or millions of
            API calls a month, with the level of technical support you need.</p>

            <Link to="/signup" className="btn btn__anim">Get Started</Link>

          </div>


        </div>
      </div>
    </div>
