# fuzzy-sets.cjsx
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved.

React = require 'react'

{ Grid } = require '../canvas'

# Much thanks to Sebastian Markbåge (sebmarkbage) whose great gist on
# how to do canvas drawing with ReactJS helped make this possible.

FuzzyInputs = React.createClass

  displayName: 'Fuzzy Input sets'

  componentDidMount: ->
    {width, height, padding, sets, changeHandler} = @props
    grid = new Grid(width, height, padding, sets, changeHandler)
    grid.initialize()

  render: ->
    {width, height} = @props
    <canvas id="canvas" width={width} height={height} />


module.exports = FuzzyInputs
