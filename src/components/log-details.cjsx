React = require 'react'
_ = require 'lodash'

{ Link } = require 'react-router'
{ connect } = require 'react-redux'
{ fetchLog } = require '../actions/logs'
{ sigfigs, fmt } = require '../sigfigs'
FuzzySetMembership = require './fuzzy-set-membership'
FuzzyOutput = require './fuzzy-output'

LogDetails = React.createClass

  displayName: 'Log Details'

  componentDidMount: ->
    { dispatch, params } = @props
    dispatch(fetchLog(params.reqID))

  # coffeelint: disable=max_line_length
  render: ->
    {log, inProgress, error, params} = @props

    # blech

    version = log?.version
    agent = log?.agent

    sig = sigfigs log?.input

    <div className="row app-row-bg">
      <div className="page log-details-page">
      {
        if log?
          <div className="log-wrapper stand-alone-logs">
            <header className="log-heading ">
              <span className="sup-heading upper fwb">Created on: <Link to="/log/#{log.reqID}">{(new Date(log.createdAt)).toLocaleString()}</Link></span>
              <h1 className="fwb upper">
                <Link to="/agents/#{log.agentID}">{agent?.name or "[Untitled]"}</Link>
              </h1>
              <span className="sup-heading upper fwb">
                <Link to="/#dashboard-log">Back to Agent logs</Link>
              </span>


            </header>
            <div className="agent-logs">

              <h2 className="fwb side-pad-left-40 upper log-details-section-wrapper__h2">Inputs</h2>
              { _.map log.input, (value, name) ->
                <div key="#{_.kebabCase(name)}" className="agent-logs-content agent-logs__input">
                  <div className="agent-logs-content__text">
                    <h3>{name}</h3>
                    <ul className="compressed-list">
                      <li className="compressed-list__item fwb h3">{name}</li>
                      <li className="compressed-list__item">Value: {value}</li>
                    </ul>

                    <ul className="compressed-list pad-40-0">
                      <li className="compressed-list__item"><strong>Fuzzy set memberships:</strong>
                        <ul className="compressed-list">
                          { _.map log.fuzzified[name], (membership, set) ->
                            if membership > 0
                              <li key="#{_.kebabCase(set)}" className="compressed-list__item">
                                <span>{set}</span>:
                                <span>{fmt(membership, sig)}</span>
                              </li>
                          }
                        </ul>
                      </li>
                    </ul>
                  </div>

                  <div className="agent-logs-content__chart">
                    <span className="pointer-arrow"></span>
                    <FuzzySetMembership
                      width="800"
                      height="240"
                      padding="30"
                      value={value}
                      sets={version.inputs[name]}
                      memberships={log.fuzzified[name]}/>
                  </div>
                </div>
              }
          </div>
          <div className="agent-logs">
            <h2 className="fwb upper log-details-section-wrapper__h2">Rules</h2>
            <div className="log-rules">
              <div className="log-rules-inner">
                  { _.map log.rules, (i) ->
                    <p key="rule-#{i}">{version.rules[i]}</p>
                  }

              </div>
            </div>
          </div>
            <div className="agent-logs">
              <h2 className="fwb upper">Output</h2>
              { _.map log.crisp, (value, name) ->
                if name != "meta"
                  <div className="agent-logs-content">
                    <div key="#{_.kebabCase(name)}" className="agent-logs-content__text">

                    <ul className="compressed-list">
                      <li className="compressed-list__item fwb h3">{name}</li>
                      <li className="compressed-list__item">Value: {fmt(value, sig)}</li>
                      <li className="compressed-list__item"><strong>Fuzzy set memberships:</strong></li>
                    </ul>
                    <ul className="compressed-list">
                      { _.map log.inferred[name], (membership, set) ->
                        if membership > 0
                          <li key="#{_.kebabCase(set)}" className="compressed-list__item">
                            <span>{set}</span>:
                            <span>{fmt(membership, sig)}</span>
                          </li>
                      }
                    </ul>

                    </div>
                    <div className="agent-logs-content__chart">
                    <span className="pointer-arrow"></span>
                      <FuzzyOutput
                        width={800}
                        height={240}
                        padding={20}
                        clipped={log.clipped[name]}
                        combined={log.combined[name]}
                        centroid={log.centroid[name]} />
                    </div>
                  </div>
              }
            </div>
          </div>
        else if inProgress?
          <p>Fetching log {params.reqID}...</p>
        else if error?
          <p>{error.message}</p>
      }
      </div>
    </div>

mapStateToProps = (state) ->
  inProgress: state.logs.inProgress
  log: state.logs.current
  error: state.logs.error

module.exports = connect(mapStateToProps)(LogDetails)
