React = require 'react'
{ render } = require 'react-dom'
{ Router, Route, IndexRoute, Redirect, browserHistory } = require 'react-router'

App = require './app'
NotFound = require './404'
About = require './about'
DynamicPricing = require './dynamic-pricing-api'
TOS = require './tos'
Privacy = require './privacy'
Login = require './login'
Home = require './home'
Pricing = require './pricing'
Upgrade = require './upgrade'
Upgraded = require './upgraded'
Signup = require './signup'
SignupComplete = require './signup-complete'
Confirmed = require './confirmed'
Reset = require './reset'
ResetRequested = require './reset-requested'
ResetPassword = require './reset-password'
ResetPasswordChanged = require './reset-password-changed'
Settings = require './settings'
HowItWorks = require './how-it-works'
MoreLogs = require './more-logs'
InvoicesDetail = require './invoices-detail'

Agents = require './agents'
AgentsEdit = require './agents-edit'
AgentsRules = require './agents-edit-rules'
AgentsSettings = require './agents-edit-settings'
AgentsDelete = require './agents-edit-confirm-delete'
AgentsStats = require './agents-stats'
AgentsTest = require './agents-test'
AgentsLogs = require './agents-logs'
NewAgent = require './new-agent'
NewAgentEmpty = require './new-agent-empty'
NewAgentTemplate = require './new-agent-template'

Tutorial = require './tutorial'
TutorialIndex = require './tutorial-index'
TutorialCreateAnAgent = require './tutorial-create-an-agent'
TutorialAddAFirstRule = require './tutorial-add-a-first-rule'
TutorialTestYourAgent = require './tutorial-test-your-agent'
TutorialIntegrateIntoYourCode = require './tutorial-integrate-into-your-code'
TutorialProvideFeedback = require './tutorial-provide-feedback'
TutorialNextSteps = require './tutorial-next-steps'

LogDetails = require './log-details'

# coffeelint: disable=max_line_length
module.exports = (store) ->
  authRequired = (location, replace) ->
    state = store.getState()

    if !state.user.authenticated
      replace('/login')

  <Route path="/" component={App}>
    <IndexRoute component={Home} />
    <Route path="about" component={About}/>
    <Route path="dynamic-pricing-api" component={DynamicPricing}/>
    <Route path="how-it-works" component={HowItWorks}/>
    <Route path="login" component={Login}/>
    <Route path="settings" component={Settings} onEnter={authRequired} />
    <Route path="tos" component={TOS}/>
    <Route path="privacy" component={Privacy}/>
    <Route path="pricing" component={Pricing}/>
    <Route path="upgrade" component={Upgrade} onEnter={authRequired} />
    <Route path="upgraded" component={Upgraded} onEnter={authRequired} />
    <Redirect from="request" to="signup" />
    <Route path="signup" component={Signup}/>
    <Route path="signup-complete" component={SignupComplete}/>
    <Route path="confirmed" component={Confirmed} />
    <Route path="reset" component={Reset}/>
    <Route path="reset-requested" component={ResetRequested}/>
    <Route path="reset-password" component={ResetPassword}/>
    <Route path="password-changed" component={ResetPasswordChanged}/>
    <Route path="logs/:page" component={MoreLogs}/>
    <route path="invoices/:invoiceId" component={InvoicesDetail} />
    <Route path="agents" component={Agents} onEnter={authRequired}>
      <Route path="new" component={NewAgent}/>
      <Route path="new/empty" component={NewAgentEmpty} />
      <Route path="new/:template" component={NewAgentTemplate} />
      <Route path=":agentID" component={AgentsEdit}>
        <IndexRoute component={AgentsRules} />
        <Route path="settings" component={AgentsSettings} />
        <Route path="stats" component={AgentsStats}/>
        <Route path="test" component={AgentsTest}/>
        <Route path="delete" component={AgentsDelete} />
        <Route path="logs/:page" component={AgentsLogs} />
      </Route>
    </Route>
    <Route path="tutorial" component={Tutorial} onEnter={authRequired}>
      <IndexRoute step={1} title="Understanding the Problem" component={TutorialIndex} />
      <Route path="create-an-agent" step={2} title="Creating an Agent" component={TutorialCreateAnAgent} />
      <Route path="add-a-first-rule" step={3} title="Add a First Rule" component={TutorialAddAFirstRule} />
      <Route path="test-your-agent" step={4} title="Test your Agent" component={TutorialTestYourAgent} />
      <Route path="integrate-into-your-code" step={5} title="Integrate into Your Code" component={TutorialIntegrateIntoYourCode} />
      <Route path="provide-feedback" step={6} title="Provide Feedback" component={TutorialProvideFeedback} />
      <Route path="next-steps" step={7} title="Next Steps" component={TutorialNextSteps} />
    </Route>
    <Route path="log/:reqID" component={LogDetails} />
    <Route path="*" component={NotFound} status={404} />
  </Route>
