React = require 'react'
{ Link } = require 'react-router'
Helmet = require 'react-helmet'

Navbar = require './navbar'
Footer = require './footer'

# coffeelint: disable=max_line_length
scripts = []

module.exports = React.createClass
  displayName: "App"

  componentDidUpdate: ->
    picturefill()

  render: ->
    <div>
      <Helmet title="fuzzy.ai"
        meta={[
          {'charset': 'utf-8'},
          {'content': 'IE=edge', 'http-equiv': 'X-UA-Compatible'},
          {'content': 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0',  'name': 'viewport'}
          {'property': 'og:url', 'content': 'https://fuzzy.ai/'}
          {'property': 'og:type', 'content': 'Website'}
          {'property': 'og:title', 'content': 'Build smarter software'}
          {'property': 'og:image', 'content': 'https://fuzzy.ai/assets/i/fuzzy-ai-logo-social-new.png'}
          {'property': 'og:description', 'content': 'Fuzzy.ai makes it easy to add intelligent decision making into any web or mobile application'}
          {'name': 'twitter:card', 'content': 'summary'}
          {'name': 'twitter:site', 'content': 'https://fuzzy.ai/'}
          {'name': 'twitter:title', 'content': 'Build smarter software' }
          {'name': 'twitter:description', 'content': 'Fuzzy.ai makes it easy to add intelligent decision making into any web or mobile application'}
          {'name': 'twitter:image', 'content': 'https://fuzzy.ai/assets/i/fuzzy-ai-logo-social-new.png'}
        ]}
        link={[
          {'rel': 'stylesheet', 'href': '/dist/style.css'},
          {'rel': 'shortcut icon', 'href': '/images/favicon.ico'}
          {'rel': 'icon', 'type': 'image/png', 'href': '/images/favicon.png'}
          ]}
        script={scripts}

      />
      <Navbar />
      {@props.children}
      <Footer />
    </div>
