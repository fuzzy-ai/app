React = require 'react'
{ Link } = require 'react-router'

module.exports = React.createClass

  displayName: "fuzzy footer nav"

  render:->
    <ul className="footer-nav">
      <li className="footer-nav__item fwb"><Link to="about">About Fuzzy.ai</Link></li>
      <li className="footer-nav__item fwb"><Link to="/tos">Terms of Service</Link></li>
      <li className="footer-nav__item fwb"><Link to="/privacy">Privacy Policy</Link></li>
    </ul>
