# fuzzy-set-membership.cjsx
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved.

React = require 'react'
_ = require 'lodash'

{ Link } = require 'react-router'
{ findDOMNode } = require 'react-dom'
centroid = require '../centroid'

# Much thanks to Sebastian Markbåge (sebmarkbage) whose great gist on
# how to do canvas drawing with ReactJS helped make this possible.

FuzzySets = React.createClass

  displayName: 'Fuzzy sets'

  componentDidMount: ->

    canvas = findDOMNode this

    # XXX: some kind of hack. Necessary? Probably not.

    # canvas.width = canvas.width

    context = canvas.getContext '2d'
    @paint context

  componentDidUpdate: ->

    {width, height} = @props

    canvas = findDOMNode this

    # XXX: some kind of hack. Necessary? Probably not.

    # canvas.width = canvas.width

    context = canvas.getContext '2d'

    context.clearRect 0, 0, width, height

    @paint context

  render: ->

    {width, height} = @props

    <canvas width={width} height={height} />

  setToPoints: (set, min) ->
    switch set.length
      when 2
        if set[0] == min
          # Scale down
          [[set[0], 0], [set[0], 1], [set[1], 0]]
        else
          # Scale up
          [[set[0], 0], [set[1], 1], [set[1], 0]]
      when 3
        # Triangle
        [[set[0], 0], [set[1], 1], [set[2], 0]]
      when 4
        # Triangle
        [[set[0], 0], [set[1], 1], [set[2], 1], [set[3], 0]]

  paint: (context) ->

    {width, height, padding, value, sets, memberships} = @props

    if _.isString width
      width = parseInt width, 10
    if _.isString height
      height = parseInt height, 10
    if _.isString padding
      padding = parseInt padding, 10
    if _.isString value
      value = parseFloat value

    # Get the min and max values in the fuzzy sets

    all = _.flatten _.values sets
    min = _.min all
    max = _.max all

    context.save()

    # Horizontal grid

    x = 0.5

    while x < width
      context.moveTo x, 0
      context.lineTo x, height
      x += 10

    # Vertical grid

    y = 0.5

    while y < height

      context.moveTo 0, y
      context.lineTo width, y
      y += 10

    context.strokeStyle = '#E5E5E5'
    context.stroke()

    # Top and bottom lines

    context.beginPath()
    context.moveTo 0, padding
    context.lineTo width, padding
    context.moveTo 0, height - padding
    context.lineTo width, height - padding
    context.strokeStyle = '#694e7c'
    context.stroke()

    # Utilities for scaling

    sX = (x) ->
      r = padding + (x - min) / (max - min) * (width - 2 * padding)

    sY = (y) ->
      r = padding + (height - 2 * padding) * (1 - y)

    # Scale lines

    raw = (max - min) / 20.0
    smooth = undefined

    # XXX: handle < 1

    n = 1
    while n < raw
      n *= 10
    if n / raw > 1
      n /= 2
    j = 0
    while j <= max - min
      context.beginPath()
      context.moveTo sX(j), sY(0.05)
      context.lineTo sX(j), sY(-0.05)
      context.strokeStyle = '#45384b'
      context.strokeWidth = 1
      context.stroke()
      context.textAlign = 'center'
      context.textBaseline = 'top'
      context.fillText j.toString(), sX(j), sY(-0.06)
      j += n
    i = 0
    colours = [
      [105, 78, 124]
      [83, 180, 196]
      [234, 70, 85]
      [69, 56, 75]
      [249, 86, 79]
      [105, 78, 124]
      [83, 180, 196]
      [225,47,62]
      [255,250,227]
      [231,76,60]
      [237.38,71]
    ]
    for setName, set of sets

      if memberships[setName] > 0
        context.beginPath()
        [r, g, b] = colours[i % colours.length]
        a = 0.75
        context.fillStyle = "rgba(#{r},#{g},#{b},#{a})"
        switch set.length
          when 2
            if set[0] == min
              # Scale down
              context.moveTo 0.5, sY(0)
              context.moveTo 0.5, sY(1)
              context.lineTo sX(set[0]), sY(1)
              context.lineTo sX(set[1]), sY(0)
              context.lineTo 0.5, sY(0)
            else
              # Scale up
              context.moveTo width - 0.5, sY(0)
              context.lineTo sX(set[0]), sY(0)
              context.lineTo sX(set[1]), sY(1)
              context.lineTo width - 0.5, sY(1)
              context.moveTo width - 0.5, sY(0)
          when 3
            # Triangle
            context.moveTo sX(set[0]), sY(0)
            context.lineTo sX(set[1]), sY(1)
            context.lineTo sX(set[2]), sY(0)
            context.lineTo sX(set[0]), sY(0)
          when 4
            # Triangle
            context.moveTo sX(set[0]), sY(0)
            context.lineTo sX(set[1]), sY(1)
            context.lineTo sX(set[2]), sY(1)
            context.lineTo sX(set[3]), sY(0)
            context.lineTo sX(set[0]), sY(0)
        context.strokeStyle = '#45384b'
        context.stroke()
        context.fill()
        # Draw the name at the center... kind of
        context.fillStyle = '#694e7c'
        context.font = '14px sans-serif'
        context.textAlign = 'center'
        context.textBaseline = 'middle'
        cpt = centroid @setToPoints set, min
        context.fillText setName, sX(cpt[0]), sY(cpt[1])
      i++

    # Line showing the input value

    context.setLineDash([5, 10])
    context.beginPath()
    context.moveTo sX(value), sY(0)
    context.lineTo sX(value), sY(1)
    context.strokeStyle = '#45384b'
    context.stroke()

    context.restore()

module.exports = FuzzySets
