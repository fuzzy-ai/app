React = require 'react'
{ Link } = require 'react-router'

Clipboard = require './clipboard'

NewApiKey = React.createClass
  displayName: 'NewApiKey'

  handleSubmit: (e) ->
    e.preventDefault()
    data =
      name: @refs.name.value

    @props.submitHandler(data)

  # coffeelint: disable=max_line_length
  render: ->
    <form onSubmit={@handleSubmit} className="user-forms">
      <label for="name">Name</label>
      <input ref="name" type="text" id="name" required="required" />
      <button className="btn btn-default">Add</button>
    </form>

module.exports = React.createClass
  displayName: 'ApiKeysSettings'

  getInitialState: ->
    showNewForm: false

  handleClick: (e) ->
    e.preventDefault()
    @setState {showNewForm: not @state.showNewForm}

  # coffeelint: disable=max_line_length
  render: ->
    <div className="module api-module">
      <h2>API Keys</h2>
      {@props.tokens?.map (token, i) ->
        <div key={i} className="setting-api-key">
          {if not token.name
            <span className="line upper small-text mbt spaced-small">Default: </span>
          else
            <span className="line small-text mbt spaced-small">{token.name} </span>
          }
          <Clipboard copyText={token.token} />
        </div>
      }
      <br />
      <div><a onClick={@handleClick} className="upper fwb small-text">Generate New API Key &raquo;</a></div>
      { if @state.showNewForm
        <NewApiKey submitHandler={@props.handleNewToken}/>
      }
    </div>
