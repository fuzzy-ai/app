React = require 'react'
{ Link } = require 'react-router'

module.exports = React.createClass
  displayName: "Request Complete"

  # coffeelint: disable=max_line_length
  render: ->
    <div className="row">
      <main className="Page_intro_zone main">
        <div className=" page_title_wrap ">
          <h1 className="page_title">Thanks!</h1>
          <p>We&apos;ve received your request.</p>
          <p>We&apos;re trying to get developers on-boarded to the beta as soon as possible.</p>
          <p>Thanks for your patience!</p>
        </div>
      </main>
    </div>
