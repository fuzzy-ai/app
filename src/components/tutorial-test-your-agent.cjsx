React = require 'react'
{ Link } = require 'react-router'
{ connect } = require 'react-redux'
{ push } = require 'react-router-redux'
{ fetchAgent } = require '../actions/agent'
{ updateTutorial } = require '../actions/tutorial'

{ BeakerIcon } = require './icons'
AgentsTest = require './agents-test'
Checklist = require './tutorial-checklist'

TutorialTestYourAgent = React.createClass
  displayName: "Tutorial - Test your Agent"

  componentDidMount: ->
    { dispatch } = @props
    dispatch updateTutorial({step: 'test-your-agent'})
    if not @props.agent?.id and @props.tutorial?.agentID
      dispatch fetchAgent(@props.tutorial.agentID)
    else if not @props.tutorial?.agentID
      dispatch push('/tutorial/create-an-agent')

    tutToggle = document.querySelectorAll('.tut-toggle')
    tutExCont = document.querySelectorAll('.tut-extra-content')

    reset = ->
      tutExCont.forEach (content) ->
        if content.classList.contains('not-hidden')
          content.classList.remove 'not-hidden'
          content.classList.add 'hidden'
        return
      return

    openUp = (e) ->
      el = e?.currentTarget
      target = el?.nextElementSibling
      if target?.classList.contains('hidden')
        reset()
        el?.classList.add 'tut-toggle-on'
        target?.classList.remove 'hidden'
        target?.classList.add 'not-hidden'
      else
        el?.classList.remove 'tut-toggle-on'
        target?.classList.add 'hidden'
        target?.classList.remove 'not-hidden'
      return

    tutToggle.forEach (toggle) ->
      toggle?.addEventListener 'click', openUp, false
      return

  # coffeelint: disable=max_line_length
  render: ->
    <div className="page">
      <div className="tutorial">
      <div className="tutorial__intro-text">
        <h1>Test your agent</h1>
        <p>You can test your agent to see what kind of results it gives.</p>
      </div>
        <div className="tutorial__row">
          <div className="tutorial__text lined-steps">
            <p className="tut-toggle"><span className="num tut-step">01.</span> In the field under <strong>&ldquo;Temperature&rdquo;</strong> enter 105</p>
            <div className="tut-extra-content hidden">
            You can enter any value here. In the test interface, you can see how your agent responds to different input values.
            </div>
            <p><span className="num tut-step">02.</span> Click the <strong>Test Your Agent</strong> button and see what result you get</p>
            <p><span className="num tut-step">03.</span> Try some other values and see what results you get.</p>
            <p><span className="num tut-step">04.</span> Go to the next step to learn about how you can integrate Fuzzy.ai into your code.</p>


          </div>
          <figure className="tutorial__image">
            <AgentsTest />
          </figure>
        </div>
        <div className="pagination tuts-pagination">
          <Link to="/tutorial/integrate-into-your-code" className="btn btn-blue">Go to step five: <strong>Integrate into your code</strong></Link>
          <br />
          <Link to="/tutorial/add-a-first-rule" className="secondary-tut-link previous-page-link">Go to previous page</Link>
          <Link to="/agents/new" className="secondary-tut-link">Skip tutorial</Link>
        </div>
      </div>
    </div>

mapStateToProps = (state) ->
  agent: state.agent.current
  tutorial: state.tutorial

module.exports = connect(mapStateToProps)(TutorialTestYourAgent)
