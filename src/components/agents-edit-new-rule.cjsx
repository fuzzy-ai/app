React = require 'react'
{ connect } = require 'react-redux'

{ addRule, storeAgent } = require '../actions/agent'

Dropdown = require './dropdown'
DecreasesRule = require './agents-edit-rule-decreases'
IfThenRule = require './agents-edit-rule-if-then'
IncreaseRule = require './agents-edit-rule-increases'

AgentsEditNewRule = React.createClass
  displayName: "New Rule"

  getInitialState: ->
    ruleType: ''
    rule:
      weight: 1.0

  handleTypeChange: (value) ->
    @setState ruleType: value
    { rule } = @state
    rule.type = value
    @setState rule: rule

  addNewRule: (agent, rule) ->
    { dispatch } = @props
    dispatch(addRule(agent, rule))
    @setState @getInitialState()

  handleStoreAgent: (agent) ->
    { dispatch } = @props
    dispatch(storeAgent(agent))

  # coffeelint: disable=max_line_length
  render: ->
    { ruleType, rule } = @state
    { agent } = @props

    options = [
      {value: "increases", label: "Increases"},
      {value: "decreases", label: "Decreases"},
      {value: "if-then", label: "If ... Then"}
    ]

    <div className="agent__new-rule">
      { switch ruleType
        when 'increases'
          <IncreaseRule rule={rule} agent={agent} submitHandler={@addNewRule} isNew=true />
        when 'decreases'
          <DecreasesRule rule={rule} agent={agent} submitHandler={@addNewRule} isNew=true  />
        when 'if-then'
          <IfThenRule rule={rule} agent={agent} submitHandler={@addNewRule} isNew=true agentHandler={@handleStoreAgent} />
      }
      <Dropdown title="Add new rule" options={options} selectHandler={@handleTypeChange} />
    </div>

mapStateToProps = (state) ->
  agent: state.agent.current

module.exports = connect(mapStateToProps)(AgentsEditNewRule)
