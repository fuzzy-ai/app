# authclient.coffee
# Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

_ = require 'lodash'
request = require 'request'

class ClientError extends Error
  constructor: (@message, @statusCode) ->
    @name = "ClientError"
    Error.captureStackTrace(this, ClientError)

class ServerError extends Error
  constructor: (@message, @statusCode) ->
    @name = "ServerError"
    Error.captureStackTrace(this, ServerError)

class AuthClient

  constructor: (@authServer, @authKey, @caCert = null) ->

    full = (rel) =>
      @authServer + rel

    handle = (verb) =>

      (rel, body, token, callback) =>

        if _.isFunction body # rel, callback
          callback = body
          token = @authKey
          body = null
        else if _.isFunction token # rel, body, callback
          callback = token
          token = @authKey

        options =
          method: verb
          url: full rel
          json: body or true
          headers:
            authorization: "Bearer #{token}"

        if @caCert
          options.agentOptions =
            ca: @caCert

        request options, (err, response, body) ->

          if err
            callback err
          else if response.statusCode >= 400 and response.statusCode < 500
            callback new ClientError(body.message or body, response.statusCode)
          else if response.statusCode >= 500 and response.statusCode < 600
            callback new ServerError(body.message or body, response.statusCode)
          else
            callback null, body

    post = handle "POST"
    get = handle "GET"
    put = handle "PUT"

    @register = (email, password, couponCode, subscribe, callback) ->
      if !callback?
        callback = subscribe
        subscribe = false
      if !callback?
        callback = couponCode
        couponCode = undefined
      props = {email: email, password: password, subscribe: subscribe}
      if couponCode? and couponCode != ''
        props.couponCode = couponCode
      post "/register", props, callback

    @login = (email, password, callback) ->
      post "/login", {email: email, password: password}, callback

    @logout = (token, callback) ->
      post "/logout", null, token, callback

    @confirm = (code, callback) ->
      post "/confirm", {code: code}, callback

    @resendConfirmation = (email, callback) ->
      post "/resend-confirmation", {email: email}, callback

    @request = (email, callback) ->
      post "/request", {email: email}, callback

    @getCoupon = (code, callback) ->
      get "/coupon/#{code}", callback

    @requestReset = (email, callback) ->
      post "/request-reset", {email: email}, callback

    @reset = (code, password, callback) ->
      post "/reset", {code: code, password: password}, callback

    @updateUser = (props, userID, callback) ->
      put "/user/#{userID}", props, callback

    @changePassword = (userID, props, callback) ->
      post "/user/#{userID}/change-password", props, callback

    @getTokens = (token, callback) ->
      get '/tokens', null, token, callback

    @newToken = (name, token, callback) ->
      post '/tokens', {name: name}, token, callback

module.exports = AuthClient
