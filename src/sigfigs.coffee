# sigfigs.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

_ = require 'lodash'
d3 = require 'd3-format'

module.exports.sigfigs = (inputs) ->
  sf = _.map inputs, (number) ->
    # Convert to exponential notation. This lets the number
    # class figure out the number of sigfigs
    ns = (new Number(number)).toExponential()
    [mantissa, exponent] = ns.split("e")
    # Get the number of digits in the mantissa (does not include . or -)
    mantissa.replace(/[^0-9]/g,"").length
  _.max sf

module.exports.fmt = (number, sigs) ->
  d3.format(",.#{sigs}r")(number) unless not sigs
