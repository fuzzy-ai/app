# src/reducers/tutorial.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

_ = require 'lodash'

initialState =
  step: null
  agentID: null
  completed: false

module.exports = (state = initialState, action) ->
  newState = switch (action?.type)
    when 'SET_TUTORIAL'
      inProgress: true
    when 'SET_TUTORIAL_SUCCESS'
      inProgress: false
      agentID: action.agentID
      step: action.step
      completed: action.completed
    when 'SET_TUTORIAL_ERROR'
      inProgress: false
      error: action.error

  if newState?
    _.assign {}, state, newState
  else
    state
