# src/reducers/logs.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

_ = require 'lodash'

initialState =
  inProgress: false
  offset: null
  limit: null
  logs: null
  agentID: null
  error: null
  reqID: null
  current: null

module.exports = (state = initialState, action) ->
  newState = switch (action?.type)
    when 'REQUEST_LOGS'
      inProgress: true
      offset: action.offset
      limit: action.limit
    when 'REQUEST_AGENT_LOGS'
      inProgress: true
      offset: action.offset
      limit: action.limit
      agentID: action.agentID
      logs: action.logs
    when 'RECEIVE_LOGS'
      inProgress: false
      offset: action.offset
      limit: action.limit
      logs: action.logs
    when 'LOGS_ERROR'
      inProgress: false
      offset: action.offset
      limit: action.limit
      error: action.err
    when 'REQUEST_LOG'
      inProgress: true
      reqID: action.reqID
    when 'RECEIVE_LOG'
      inProgress: false
      reqID: action.reqID
      current: action.current
    when 'LOG_ERROR'
      inProgress: false
      reqID: action.reqID
      error: action.err

  if newState?
    _.assign {}, state, newState
  else
    state
