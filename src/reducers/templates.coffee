# src/reducers/templates.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

_ = require 'lodash'

initialState =
  inProgress: false
  templates: []
  error: null

module.exports = (state = initialState, action) ->
  newState = switch (action.type)
    when 'REQUEST_TEMPLATES'
      inProgress: true
      error: null
    when 'RECEIVE_TEMPLATES'
      inProgress: false
      templates: action.templates
      error: null
    when 'TEMPLATES_ERROR'
      inProgress: false
      templates: []
      error: action.error

  if newState?
    _.assign {}, state, newState
  else
    state
