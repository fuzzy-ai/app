# src/reducers/agent.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

_ = require 'lodash'

initialState =
  inProgress: false
  items: []
  templates: []
  current: {}
  error: null
  timing: null
  tutorial: null

module.exports = (state = initialState, action) ->
  newState = switch (action?.type)
    when 'AGENT_CREATE_BEGIN'
      inProgress: true
    when 'AGENT_CREATE_SUCCESS'
      inProgress: false
      current: action.agent
      error: null
    when 'AGENT_CREATE_ERROR'
      inProgress: false
      current: action.agent
      error: action.error
    when 'REQUEST_AGENTS'
      inProgress: true
    when 'RECEIVE_AGENTS'
      inProgress: false
      items: action.agents
      current: {}
      error: null
    when 'REQUEST_AGENTS_ERROR'
      inProgress: false
      items: []
      error: action.error
    when 'REQUEST_AGENT_BEGIN'
      inProgress: true
    when 'REQUEST_AGENT_SUCCESS'
      inProgress: false
      current: action.agent
      error: null
    when 'REQUEST_AGENT_ERROR'
      inProgress: false
      error: action.error
      current: null
    when 'STORE_AGENT_BEGIN'
      inProgress: true
    when 'STORE_AGENT_SUCCESS'
      inProgress: false
      current: action.agent
      error: null
    when 'STORE_AGENT_ERROR'
      inProgress: false
      error: action.error
      current: action.agent
    when 'DELETE_AGENT_ERROR'
      inProgress: false
      error: action.error
      current: action.agent
    when 'TEST_AGENT_BEGIN'
      inProgress: true
      current: action.agent
      inputs: action.inputs
    when 'TEST_AGENT_SUCCESS'
      inProgress: false
      current: action.agent
      inputs: action.inputs
      outputs: action.outputs
      meta: action.meta
      timing: action.timing
      error: null
    when 'TEST_AGENT_ERROR'
      inProgress: false
      current: action.agent
      inputs: action.inputs
      outputs: null
      meta: null
      error: action.error
    when 'TEST_AGENT_RESET'
      inProgress: false
      current: action.agent
      inputs: null
      outputs: null
      timing: null
      meta: null

  if newState?
    _.assign {}, state, newState
  else
    state
