{ combineReducers } = require 'redux'
{ routerReducer } = require 'react-router-redux'
{ requestsReducer } = require 'redux-requests'

agent = require './agent'
logs = require './logs'
payments = require './payments'
plans = require './plans'
stats = require './stats'
templates = require './templates'
tokens = require './tokens'
tutorial = require './tutorial'
user = require './user'

module.exports = combineReducers
  agent: agent
  logs: logs
  payments: payments
  plans: plans
  requests: requestsReducer
  routing: routerReducer
  stats: stats
  templates: templates
  tokens: tokens
  tutorial: tutorial
  user: user
