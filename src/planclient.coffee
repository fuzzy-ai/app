# planclient.coffee
# Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

MicroserviceClient = require '@fuzzy-ai/microservice-client'

class PlanClient extends MicroserviceClient

  plans: (callback) ->
    @get "/plans", callback

  getPlan: (stripeID, callback) ->
    @get "/plan/#{stripeID}", callback

module.exports = PlanClient
