# Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
#
# All rights reserved.

assert = require 'assert'
_ = require 'lodash'

isPoint = (pt) ->
  _.isArray(pt) and pt.length == 2 and _.isFinite(pt[0]) and _.isFinite(pt[1])

triangleCentroid = (a, b, c) ->

  assert isPoint a
  assert isPoint b
  assert isPoint c

  ctr = [0, 0]

  ctr[0] = (a[0] + b[0] + c[0])/3
  ctr[1] = (a[1] + b[1] + c[1])/3

  assert isPoint ctr

  ctr

triangleArea = (a, b, c) ->

  assert isPoint a
  assert isPoint b
  assert isPoint c

  if a[0] == b[0] and a[1] > b[1] # downslope
    h = a[1] - b[1]
    w = c[0] - b[0]
  else if b[0] == c[0] and c[1] > b[1] # upslope
    h = c[1] - b[1]
    w = b[0] - a[0]

  area = h * w / 2

  assert _.isFinite area

  area

rectangleCentroid = (a, b) ->

  assert isPoint a
  assert isPoint b

  assert.equal a[1], b[1]

  ctr = [0, 0]

  ctr[0] = (a[0] + b[0])/2
  ctr[1] = a[1]/2

  assert isPoint ctr

  ctr

rectangleArea = (a, b) ->

  assert isPoint a
  assert isPoint b

  assert.equal a[1], b[1]

  h = a[1]
  w = b[0] - a[0]

  area = h * w

  assert _.isFinite area

  area

centroid = (points) ->

  # Format: an array of points

  assert.ok _.isArray points

  # Each point is a 2-number array

  _.each points, (point) ->
    assert isPoint(point)

  # First and last point are on the X-axis

  assert.equal points[0][1], 0, "First point not on X axis: #{JSON.stringify(points)}"
  assert.equal points[points.length - 1][1], 0, "Last point not on X axis: #{JSON.stringify(points)}"

  centroids = []
  areas = []

  _.each points, (point, i) ->
    if i == points.length - 1
      return
    next = points[i+1]
    if point[0] == next[0] # vertical line
      return
    else if point[1] == next[1] # horizontal line
      centroids.push rectangleCentroid point, next
      areas.push rectangleArea point, next
    else if point[1] > next[1] # negative slope
      centroids.push triangleCentroid point, [point[0], next[1]], next
      areas.push triangleArea point, [point[0], next[1]], next
      if next[1] > 0
        centroids.push rectangleCentroid [point[0], next[1]], next
        areas.push rectangleArea [point[0], next[1]], next
    else if point[1] < next[1] # positive slope
      centroids.push triangleCentroid point, [next[0], point[1]], next
      areas.push triangleArea point, [next[0], point[1]], next
      if point[1] > 0
        centroids.push rectangleCentroid point, [next[0], point[1]]
        areas.push rectangleArea point, [next[0], point[1]]

  assert.equal areas.length, centroids.length

  add = (sum, area) -> sum + area

  totalArea = _.reduce areas, add, 0

  wx = wy = 0

  _.each centroids, (ctr, i) ->
    wx += ctr[0] * areas[i]
    wy += ctr[1] * areas[i]

  ctr = [wx/totalArea, wy/totalArea]

  assert isPoint ctr

  ctr

module.exports = centroid
